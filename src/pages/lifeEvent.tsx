import React, { useEffect } from 'react';
import { LifeEventPage } from '@persoonlijke-regelingen-assistent/components-react';
import birthdayPartyImg from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { selectItem, fetch } from '../state/items/itemsSlice';
import { type LifeEvent, type Item } from '../state/items/itemsInterface';

import { useHistory } from 'react-router';
import htmlContentBlockRenderer from '../functions/htmlContentBlockRenderer';
import { saveOrRemoveItem, isItemSaved } from '../state/savedItems/savedItemsSlice';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';

function lifeEvent({ id }: { id: string }): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const history = useHistory();

  useEffect(() => {
    void dispatch(fetch(Number(id)));
  }, []);

  const isBookmarked = useAppSelector(isItemSaved({ id: Number(id), type: 'lifeEvent' }));

  const isLifeEvent = (item: Item | undefined): item is LifeEvent =>
    item != null && Object.keys(item).includes('Levensgebeurtenis.Id');
  const item = useAppSelector(selectItem(Number(id)));

  if (!isLifeEvent(item)) {
    return null;
  } else {
    const lifeEvent = item;
    const content = lifeEvent?.['Levensgebeurtenis.Content']?.map(
      ({ ContentBlocks = [], ContentType, ContentId, Label }) => {
        const ContentElements = ContentBlocks.map(({ ContentBlockType, Body }) => {
          if (ContentBlockType === 'HtmlContentBlock') {
            return htmlContentBlockRenderer(Body);
          }
          return null;
        });
        return { ContentType, ContentId, Label, ContentElements };
      },
    );
    const title = lifeEvent?.['Zoekdescriptor.Titel'];
    const organisation = lifeEvent?.Wordt_Onderhouden_Door;
    const description = lifeEvent?.['Levensgebeurtenis.Omschrijving'];
    const id = lifeEvent?.Id;
    const baseURL = import.meta.env?.BASE_URL ?? '/';
    return (
      <LifeEventPage
        navigationBar={<MenuNavigation selectedItem="Dashboard" indicators={[]} />}
        baseURL={baseURL}
        bookmarkAction={() => {
          dispatch(
            saveOrRemoveItem({
              id,
              type: 'lifeEvent',
              label: title,
              imageSrc: birthdayPartyImg,
            }),
          );
        }}
        lifeEvent={{
          title,
          id,
          organisation,
          description,
          content,
          isBookmarked,
        }}
      />
    );
  }
}

export default lifeEvent;
