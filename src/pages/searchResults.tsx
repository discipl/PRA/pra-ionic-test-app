import React, { type CSSProperties, useEffect, useState } from 'react';
import {
  Button,
  ButtonBadge,
  ButtonGroup,
  GridLayout,
  GridLayoutCell,
  Paragraph,
  Textbox,
  SearchResultsList,
  SearchResultsItem,
  Icon,
  LabelBadge,
  Link,
  Heading,
} from '@persoonlijke-regelingen-assistent/components-react';
import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import X from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/X';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import {
  search,
  selectActiveFilterId,
  selectSearchResults,
  setActiveFilterAndSearch,
  upsertUnsavedFilterAndSearch,
} from '../state/searchResults/searchResultsSlice';
import { Page } from '../components/Page/Page';
import { selectSearchFilter, selectSearchFilterNames } from '../state/searchFilters/searchFiltersSlice';

import { useHistory } from 'react-router';
import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import { type SearchFilter } from '../state/searchFilters/searchFiltersInterface';

function searchResults(): ReturnType<React.FC> {
  const [searchQuery, setSearchQuery] = useState<string>('');
  const searchResults = useAppSelector(selectSearchResults);
  const searchFilterNames = useAppSelector(selectSearchFilterNames);
  const activeFilterId = useAppSelector(selectActiveFilterId);
  let searchFilter: SearchFilter | undefined;
  if (activeFilterId != null) {
    searchFilter = useAppSelector(selectSearchFilter(activeFilterId));
  }
  const { results = [], totalResults, themes = [], searchquery = '' } = searchResults;
  const dispatch = useAppDispatch();
  const history = useHistory();

  const handleKeyDown = (event: React.KeyboardEvent): void => {
    if (event.key === 'Enter') {
      void dispatch(
        search({
          themes: searchFilter?.themes,
          searchquery: searchQuery,
        }),
      );
    }
  };

  useEffect(() => {
    void dispatch(
      search({
        themes: searchFilter?.themes,
        searchquery: searchQuery,
      }),
    );
  }, []);

  const removeFilter = (filterLabel: string, filterType: keyof SearchFilter): void => {
    const _searchFilter = structuredClone(searchFilter);
    if (_searchFilter != null) {
      const currentValue = _searchFilter?.[filterType];
      if (Array.isArray(currentValue)) {
        const newValue: undefined | string[] =
          currentValue?.filter((label: string) => label !== filterLabel) ?? undefined;
        if (
          filterType === 'themes' ||
          filterType === 'localAuthorities' ||
          filterType === 'provincialAuthorites' ||
          filterType === 'regionalWaterAuthorites'
        ) {
          _searchFilter[filterType] = newValue;
        }
      } else if (typeof currentValue === 'string') {
        _searchFilter[filterType] = undefined;
      }
      void dispatch(
        upsertUnsavedFilterAndSearch({
          searchFilter: _searchFilter,
          searchquery: searchQuery,
        }),
      );
    }
  };

  const activeFilters: Array<{ filterLabel: string; filterType: keyof SearchFilter }> = [
    ...(searchFilter?.themes != null
      ? searchFilter?.themes.map((filterLabel) => ({ filterLabel, filterType: 'themes' as keyof SearchFilter }))
      : []),
    ...(searchFilter?.regionalWaterAuthorites != null
      ? searchFilter?.regionalWaterAuthorites.map((filterLabel) => ({
          filterLabel,
          filterType: 'regionalWaterAuthorites' as keyof SearchFilter,
        }))
      : []),
    ...(searchFilter?.localAuthorities != null
      ? searchFilter?.localAuthorities.map((filterLabel) => ({
          filterLabel,
          filterType: 'localAuthorities' as keyof SearchFilter,
        }))
      : []),
    ...(searchFilter?.provincialAuthorites != null
      ? searchFilter?.provincialAuthorites.map((filterLabel) => ({
          filterLabel,
          filterType: 'provincialAuthorites' as keyof SearchFilter,
        }))
      : []),
    ...(searchFilter?.workSituation != null
      ? [{ filterLabel: searchFilter?.workSituation, filterType: 'workSituation' as keyof SearchFilter }]
      : []),
    ...(searchFilter?.entity != null
      ? [{ filterLabel: searchFilter?.entity, filterType: 'entity' as keyof SearchFilter }]
      : []),
    ...(searchFilter?.livingSituation != null
      ? [{ filterLabel: searchFilter?.livingSituation, filterType: 'livingSituation' as keyof SearchFilter }]
      : []),
  ].filter((filter) => {
    if (filter?.filterLabel != null && filter?.filterType != null) {
      return filter;
    }
    return null;
  });
  const searchedOnTheme =
    themes.length === 1 && activeFilters.length === 1 && searchquery.length === 0 ? themes[0] : undefined;
  return (
    <Page
      headerTitle="Zoeken"
      navigationSelectedItem="Dashboard"
      headerBackButtonAction={() => {
        history.goBack();
      }}
    >
      <GridLayout templateColumns={6}>
        <GridLayoutCell columnSpan={6}>
          <Textbox
            placeholder="Zoekterm"
            onChange={(e) => {
              setSearchQuery(e.target.value);
            }}
            onKeyDown={(e) => {
              handleKeyDown(e);
            }}
            iconStart={Search({})}
            invalid={false}
          />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6} justifyContent="end" alignItems="end">
          <ButtonGroup>
            <Button
              appearance="subtle-button"
              onClick={() => {
                history.push('/search-filter/new');
              }}
            >
              +
            </Button>
            {searchFilterNames.map(({ id, name }) => {
              if (id === '_unsavedFilters') {
                return null;
              }
              return (
                <ButtonBadge
                  appearance="primary"
                  key={id}
                  pressed={id === activeFilterId}
                  onClick={() => {
                    if (id != null) {
                      void dispatch(
                        setActiveFilterAndSearch({
                          activeFilterId: activeFilterId != null && id === activeFilterId ? undefined : id,
                          searchquery: searchQuery,
                        }),
                      );
                    }
                  }}
                >
                  {name}
                </ButtonBadge>
              );
            })}
          </ButtonGroup>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6} alignItems="end">
          <Link
            onClick={() => {
              if (activeFilterId != null) {
                history.push(`/search-filter/${activeFilterId}`);
              } else {
                history.push(`/search-filter/`);
              }
            }}
            style={{ display: 'flex', 'align-items': 'center' } satisfies CSSProperties}
          >
            <Icon style={{ cursor: 'pointer' }}>{ChevronDown({})}</Icon>Alle filters
          </Link>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Paragraph small style={{ color: 'var(--pra-color-primary-400)' }}>
            {totalResults} resultaten
          </Paragraph>
          {totalResults != null && (
            <>
              <Link>{`Resultaten voor ${searchedOnTheme != null ? 'thema' : ''}`}</Link>
              <Heading level={2}>{`"${searchedOnTheme ?? searchquery}"`}</Heading>
            </>
          )}
          <ButtonGroup>
            {activeFilters.map((filter) => {
              const { filterLabel, filterType }: { filterLabel: string; filterType: keyof SearchFilter } = filter;
              return (
                <LabelBadge
                  key={filterLabel}
                  variant={`${activeFilterId === '_unsavedFilters' ? 'primary' : 'secondary'}`}
                >
                  {filterLabel}
                  {
                    <Icon
                      onClick={() => {
                        removeFilter(filterLabel, filterType);
                      }}
                    >
                      {X({})}
                    </Icon>
                  }
                </LabelBadge>
              );
            })}
          </ButtonGroup>
        </GridLayoutCell>
        {totalResults != null && totalResults > 0 ? (
          <GridLayoutCell columnSpan={6} paddingInline={0}>
            <SearchResultsList>
              {results.map((searchResult) => {
                return (
                  <SearchResultsItem
                    key={searchResult.title}
                    title={searchResult.title}
                    subTitle={searchResult.authority}
                    description={searchResult.descriptionShort}
                    onClick={() => {
                      history.push(`/regulations/${searchResult.id}`);
                    }}
                  />
                );
              })}
            </SearchResultsList>
          </GridLayoutCell>
        ) : (
          <GridLayoutCell columnSpan={6}>
            <Heading level={1}>Helaas er zijn geen regelingen gevonden</Heading>
            <Paragraph>
              Probeer het opnieuw. Je kan zoeken op de volgende manieren:
              <ul>
                <li>Woorden in de naam/titel van de regeling</li>
                <li>Woorden in de omschrijving van de regeling</li>
                <li>De naam van de organisatie die verantwoordelijk is voor de regeling</li>
              </ul>
            </Paragraph>
          </GridLayoutCell>
        )}
      </GridLayout>
    </Page>
  );
}

export default searchResults;
