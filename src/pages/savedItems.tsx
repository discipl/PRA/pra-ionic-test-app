import React from 'react';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import { SavedItemsPage } from '@persoonlijke-regelingen-assistent/components-react';
import { useHistory } from 'react-router';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';
import { selectSavedItems } from '../state/savedItems/savedItemsSlice';
import { useAppSelector } from '../state/hooks';
function SavedItems(): ReturnType<React.FC> {
  const history = useHistory();
  const baseURL = import.meta.env?.BASE_URL ?? '/';
  const savedItems = useAppSelector(selectSavedItems);
  const { regulations, news, lifeEvents } = savedItems;
  return (
    <SavedItemsPage
      navigationBar={<MenuNavigation selectedItem="Profiel" indicators={[]} />}
      profiles={[
        { name: 'Alles', pressed: true },
        { name: 'Loes', pressed: false },
        { name: 'Oma', pressed: false },
      ]}
      baseURL={baseURL}
      savedLifeEvents={lifeEvents}
      savedRegulations={regulations}
      savedNews={
        news.length > 0
          ? news
          : [
              {
                id: 0,
                type: 'news',
                title: 'DigiD aanvragen',
                message: 'Rijksoverheid',
                notificationMessage: 'Nog 3 dagen',
              },
            ]
      }
      regulationClickAction={(id) => {
        history.push(`/regulations/${id}`);
      }}
      lifeEventClickAction={(id) => {
        history.push(`/life-events/${id}`);
      }}
    />
  );
}

export default SavedItems;
