import Dashboard from './dashboard';
import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../state/store';
import * as fetchContent from '../endpoints/fetchContent';
import { Router } from 'react-router';
import { createMemoryHistory } from 'history';
const history = createMemoryHistory();

describe('[Dashboard]', () => {
  global.fetch = jest.fn(
    async () =>
      await Promise.resolve({
        json: async () => await Promise.resolve({}),
      }),
  ) as jest.Mock;

  describe('Rendering', () => {
    it('should be able to search using the searchbar', function () {
      render(
        <Router history={history}>
          <Provider store={store}>
            <Dashboard />
          </Provider>
        </Router>,
      );

      const sendMessageSpy = jest.spyOn(fetchContent, 'default').mockResolvedValue({
        searchquery: '',
        from: 0,
        to: 0,
        totalResults: 0,
        results: [],
      });

      const searchBar = screen.getByPlaceholderText('Waar bent u naar op zoek?');

      fireEvent.change(searchBar, { target: { value: 'TEST' } });
      fireEvent.keyDown(searchBar, {
        key: 'Enter',
        code: 'Enter',
        charCode: 13,
      });

      expect(sendMessageSpy).toBeCalledWith({
        filters: undefined,
        ids: undefined,
        n: undefined,
        searchquery: 'TEST',
        start: undefined,
      });
    });
  });
  (global.fetch as jest.Mock).mockClear();
});
