import React from 'react';
import { ProfilePage } from '@persoonlijke-regelingen-assistent/components-react';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { selectProfile, upsertProfileDataValue } from '../state/profile/profileSlice';
import { type Profile } from '../state/profile/profileInterface';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';

function ConnectedProfilePage(): ReturnType<React.FC> {
  const profileData = useAppSelector(selectProfile);
  const dispatch = useAppDispatch();

  return (
    <ProfilePage
      avatarChangeAction={() => {}}
      navigationBar={<MenuNavigation selectedItem="Instellingen" indicators={[]} />}
      onProfileDataChange={(profileKey, value: boolean | string | undefined) => {
        dispatch(upsertProfileDataValue({ key: profileKey as keyof Profile, value }));
      }}
      initialEditMode={false}
      profile={{ ...profileData, firstname: profileData.givenname }}
    />
  );
}

export default ConnectedProfilePage;
