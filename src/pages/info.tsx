import React from 'react';
import { InfoPage } from '@persoonlijke-regelingen-assistent/components-react';
import { useHistory } from 'react-router';
import { useAppDispatch } from '../state/hooks';
import { setFirstUse } from '../state/app/appSlice';

function Info(): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const history = useHistory();
  return (
    <InfoPage
      nextButtonAction={() => {
        dispatch(setFirstUse(false));
        history.push('/');
      }}
    />
  );
}

export default Info;
