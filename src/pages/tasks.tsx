import React from 'react';
import './tasks.scss';
import { Button, Accordeon, Icon } from '@persoonlijke-regelingen-assistent/components-react';
import { addTask, markCompleted, selectTasks } from '../state/tasks/tasksSlice';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { useHistory } from 'react-router';
import { Page } from '../components/Page/Page';

function Tasks(): ReturnType<React.FC> {
  const tasks = useAppSelector(selectTasks);
  const dispatch = useAppDispatch();
  const history = useHistory();

  return (
    <Page headerTitle="Taken" navigationSelectedItem="Tasks">
      <div className="tasks">
        <h1>Taken</h1>
        <div>
          <Button
            onClick={() => {
              dispatch(addTask());
            }}
          >
            +
          </Button>
        </div>
        <div className="tasks-list">
          {tasks.length > 0 &&
            tasks.map((task) => {
              const { title = '', description = '', id, completed = false, completedTimestamp = '' } = task;
              return (
                <Accordeon title={title} key={title}>
                  <p>{description}</p>
                  {completed ? (
                    <div>
                      <Icon>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          strokeWidth="2"
                          stroke="currentColor"
                          fill="none"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                        >
                          <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                          <path
                            d="M17 3.34a10 10 0 1 1 -14.995 8.984l-.005 -.324l.005 -.324a10 10 0 0 1 14.995 -8.336zm-1.293 5.953a1 1 0 0 0 -1.32 -.083l-.094 .083l-3.293 3.292l-1.293 -1.292l-.094 -.083a1 1 0 0 0 -1.403 1.403l.083 .094l2 2l.094 .083a1 1 0 0 0 1.226 0l.094 -.083l4 -4l.083 -.094a1 1 0 0 0 -.083 -1.32z"
                            strokeWidth="0"
                            fill="currentColor"
                          ></path>
                        </svg>
                      </Icon>
                      {completedTimestamp}
                    </div>
                  ) : (
                    <Icon
                      onClick={() => {
                        dispatch(markCompleted({ id }));
                      }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        strokeWidth="2"
                        stroke="currentColor"
                        fill="none"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                      >
                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                        <path d="M9 11l3 3l8 -8"></path>
                        <path d="M20 12v6a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2h9"></path>
                      </svg>
                    </Icon>
                  )}
                  <Icon
                    onClick={() => {
                      history.push(`/settings/tasks/${id}`);
                    }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      strokeWidth="2"
                      stroke="currentColor"
                      fill="none"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                      <path d="M11 12h10"></path>
                      <path d="M18 9l3 3l-3 3"></path>
                      <path d="M7 12a2 2 0 1 1 -4 0a2 2 0 0 1 4 0z"></path>
                    </svg>
                  </Icon>
                </Accordeon>
              );
            })}
        </div>
      </div>
    </Page>
  );
}

export default Tasks;
