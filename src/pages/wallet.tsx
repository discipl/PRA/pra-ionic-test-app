import React, { useEffect, useLayoutEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { isPlatform } from '@ionic/react';
import { v4 } from 'uuid';
import { decodeFirst } from 'cbor-web';
import { Buffer } from 'buffer';
import {
  type PresentationDefinitionResponse,
  type PresentationsResponse,
  type DefinitionPath,
  type ScenarioDefinition,
} from '../typescript-definitions/eudi-wallet';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';
import { WalletConnectionPage } from '@persoonlijke-regelingen-assistent/components-react';
import { environment } from '../environment/environment';
import { fetchEudiWalletResponse } from '../endpoints/fetchEudiWalletResponse';
import { initializeEudiWalletTransaction } from '../endpoints/initializeEudiWalletTransaction';
import { selectProfile, upsertProfileData } from '../state/profile/profileSlice';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { calculatePensionDate, calculateTax, selectVariables } from '../state/modelExecutor/modelExecutorSlice';

const Passport = 'eu.europa.ec.eudiw.pid.1';
const DrivingLicenseNameSpace = 'org.iso.18013.5.1';
const DrivingLicenseDocType = 'org.iso.18013.5.1.mDL';

const docTypes = { Passport: Passport, DrivingLicense: DrivingLicenseDocType };
const nameSpaces = { Passport: Passport, DrivingLicense: DrivingLicenseNameSpace };

const baseURL = import.meta.env?.BASE_URL ?? '/';

const generateDefinitionField = (
  fieldName: string,
  intentToRetain = false,
  documentType: 'DrivingLicense' | 'Passport',
): DefinitionPath => {
  return {
    path: [`$['${nameSpaces[documentType]}']['${fieldName}']`],
    intent_to_retain: intentToRetain,
  };
};

const buildScenarioDefinition = ({
  nonce,
  fields,
  documentType,
}: {
  nonce: string;
  fields: DefinitionPath[];
  documentType: 'Passport' | 'DrivingLicense';
}): ScenarioDefinition => {
  return {
    type: 'vp_token',
    presentation_definition: {
      id: '32f54163-7166-48f1-93d8-ff217bdb0653',
      input_descriptors: [
        {
          id: `${docTypes[documentType]}`,
          name: 'PRA',
          purpose: 'Koppel uw gegevens met de PRA',
          format: {
            mso_mdoc: {
              alg: ['ES256', 'ES384', 'ES512', 'EdDSA', 'ESB256', 'ESB320', 'ESB384', 'ESB512'],
            },
          },
          constraints: {
            fields,
          },
        },
      ],
    },
    nonce,
  };
};

const getWalletRequestURL = async (
  selectedFields: string[],
  documentType: 'Passport' | 'DrivingLicense',
): Promise<string> => {
  const payload = buildScenarioDefinition({
    nonce: v4(),
    fields: selectedFields.map((fieldName) => generateDefinitionField(fieldName, false, documentType)),
    documentType,
  });

  if (!isPlatform('desktop')) {
    payload.wallet_response_redirect_uri_template =
      location.origin + baseURL + 'settings/wallet' + '?wallet_response_code={RESPONSE_CODE}';
  }
  const data: PresentationDefinitionResponse = await initializeEudiWalletTransaction(payload);
  let builtURL = `${environment.eudiApiUrl}?client_id=${data.client_id}&request_uri=${encodeURIComponent(data.request_uri)}`;
  builtURL = builtURL.replace('https://', 'eudi-openid4vp://');
  localStorage.setItem('presentation_id', data.presentation_id);

  return builtURL;
};

const fetchAndDecodeWalletResponse = async (responseCode: string): Promise<Array<Record<string, string>>> => {
  const presentationId = localStorage.getItem('presentation_id') ?? '';
  const _data: PresentationsResponse = await fetchEudiWalletResponse(presentationId, responseCode);
  const buf = Buffer.from(_data.vp_token ?? '', 'base64');
  const decodedValueOut = await decodeFirst(buf);
  const dataArrayPassport = decodedValueOut.documents[0].issuerSigned.nameSpaces[nameSpaces.Passport];
  const dataArrayDrivingLicense = decodedValueOut.documents[0].issuerSigned.nameSpaces[nameSpaces.DrivingLicense];

  const pp_requestData = await Promise.all(
    [...(dataArrayPassport?.length > 0 ? dataArrayPassport : [])].map(async (item: any) => {
      return decodeFirst(item.value, { preferWeb: true });
    }),
  );
  const dl_requestData = await Promise.all(
    [...(dataArrayDrivingLicense?.length > 0 ? dataArrayDrivingLicense : [])].map(async (item: any) => {
      return decodeFirst(item.value, { preferWeb: true });
    }),
  );
  const pp_data = pp_requestData.map((item: any) => ({
    key: `pp_${item.elementIdentifier.replace(/_/g, '')}`,
    value: item.elementValue,
  }));
  const dl_data = dl_requestData.map((item: any) => ({
    key: `dl_${item.elementIdentifier.replace(/_/g, '')}`,
    value: item.elementValue,
  }));

  return [...pp_data, ...dl_data];
};

function Wallet({ walletResponseCode }: { walletResponseCode?: string }): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const profileData = useAppSelector(selectProfile);
  const calculationVariables = useAppSelector(selectVariables);

  useEffect(() => {
    if (walletResponseCode != null) {
      const decodeResponseData = async (walletResponseCode: string): Promise<void> => {
        const _decodedData = await fetchAndDecodeWalletResponse(walletResponseCode);

        const birthdate =
          (_decodedData?.find(({ key }) => key === 'pp_birthdate')?.value?.value as string) || undefined;
        const givenname = _decodedData?.find(({ key }) => key === 'pp_givenname')?.value || undefined;
        const familyname = _decodedData?.find(({ key }) => key === 'pp_familyname')?.value || undefined;
        const pension =
          JSON.parse(_decodedData?.find(({ key }) => key === 'dl_documentnumber')?.value || 'null') || undefined;
        const aow = Number(_decodedData?.find(({ key }) => key === 'dl_givenname')?.value) || undefined;
        dispatch(
          upsertProfileData({
            ...(birthdate ? { birthdate } : {}),
            ...(givenname ? { givenname } : {}),
            ...(familyname ? { familyname } : {}),
            ...(pension ? { pension } : {}),
            ...(aow ? { aow } : {}),
          }),
        );
      };

      void decodeResponseData(walletResponseCode);
    }
  }, []);

  //Component Will Unmount
  useLayoutEffect(() => {
    if (
      profileData.birthdate != null &&
      calculationVariables?.['algemene-ouderdomswet/dagenTotPensioen'] === undefined
    ) {
      void dispatch(calculatePensionDate({ birthDate: profileData.birthdate }));
    }
  }, [profileData]);

  return (
    <WalletConnectionPage
      navigationBar={<MenuNavigation selectedItem="Instellingen" indicators={[]} />}
      menuTitle="Wallet connectie"
      walletCards={[
        {
          title: 'Persoonsgegevens',
          data: [
            { fieldName: 'Achternaam', value: profileData?.familyname },
            { fieldName: 'Voornaam', value: profileData?.givenname },
            { fieldName: 'Geboortedatum', value: profileData?.birthdate },
          ],
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          dataRequestAction: async () => {
            if (!isPlatform('desktop')) {
              const _url = await getWalletRequestURL(['family_name', 'given_name', 'birth_date'], 'Passport');
              window.open(_url);
            }
          },
        },
        {
          title: 'Mijnpensioenoverzicht',
          data: [
            ...Object.keys(profileData?.pension || { ['Pensioenpotje(s)']: '' }).map((pensionItemKey) => {
              return { fieldName: pensionItemKey, value: `${profileData?.pension?.[pensionItemKey] || ''} ` };
            }),
            { fieldName: 'AOW bedrag', value: `${profileData?.aow || ''}` },
          ],
          // eslint-disable-next-line @typescript-eslint/no-misused-promises
          dataRequestAction: async () => {
            if (!isPlatform('desktop')) {
              const _url = await getWalletRequestURL(['document_number', 'given_name'], 'DrivingLicense');
              window.open(_url);
            }
          },
        },
      ]}
    />
  );
}

export default Wallet;
