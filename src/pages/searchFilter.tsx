import ChevronDown from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronDown';
import Search from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/Search';
import React, { useEffect, useState } from 'react';
import LocalAuthoritiesData from '../staticData/local-authorities.json';
import ProvincialAuthoritiesData from '../staticData/provincial-authorities.json';
import {
  Button,
  ButtonGroup,
  Checkbox,
  FormField,
  FormSelectionButtons,
  GridLayout,
  GridLayoutCell,
  Heading,
  Select,
  SelectOption,
  Textbox,
} from '@persoonlijke-regelingen-assistent/components-react';
import { Page } from '../components/Page/Page';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { upsertSearchFilter, selectSearchFilter } from '../state/searchFilters/searchFiltersSlice';
import { type SearchFilter } from '../state/searchFilters/searchFiltersInterface';
import { useHistory } from 'react-router';
import { fetchThemesList, selectThemesList } from '../state/themesList/themesListSlice';
import { type ThemesListItem } from '../state/themesList/themesListInterface';
import { setActiveFilterAndSearch } from '../state/searchResults/searchResultsSlice';

function searchFilter({ filterId }: { filterId?: string }): ReturnType<React.FC> {
  const isNewFilter = filterId === 'new';
  const [searchFilter, setSearchFilter] = useState<SearchFilter | undefined>(undefined);
  const [addNewChecked, setAddNewChecked] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const themes = useAppSelector(selectThemesList) ?? [];
  const history = useHistory();
  let isExistingFilter = false;
  let stateSearchFilter: SearchFilter | undefined;

  useEffect(() => {
    void dispatch(fetchThemesList());
  }, []);

  if (filterId != null && !isNewFilter) {
    stateSearchFilter = useAppSelector(selectSearchFilter(filterId));
    isExistingFilter = !(stateSearchFilter == null);
  }

  useEffect(() => {
    if (stateSearchFilter != null) {
      setSearchFilter(stateSearchFilter);
    }
  }, [stateSearchFilter]);

  let filterPageTitle = 'Filters';
  let filterConfirmButton = 'Filters gebruiken';
  if (isNewFilter) {
    filterPageTitle = 'Filterprofiel toevoegen';
    filterConfirmButton = 'Zoekfilterprofiel opslaan';
  } else if ((isExistingFilter && searchFilter?.id !== '_unsavedFilters') ?? addNewChecked) {
    filterPageTitle = 'Filterprofiel aanpassen';
    filterConfirmButton = 'Zoekfilterprofiel wijzigen';
  }

  const updateSearchFilterField = <K extends keyof SearchFilter>(fieldName: K, value: SearchFilter[K]): void => {
    const _searchFilter = { ...searchFilter };
    _searchFilter[fieldName] = value;
    setSearchFilter(_searchFilter);
  };

  const _nameField = (
    <GridLayoutCell columnSpan={6}>
      <Heading level={5}>Naam filterprofiel:</Heading>
      <Textbox
        invalid={false}
        value={searchFilter != null ? searchFilter.name : stateSearchFilter?.name}
        placeholder="Type hier...."
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          updateSearchFilterField('name', event?.currentTarget?.value);
        }}
      />
    </GridLayoutCell>
  );
  if (searchFilter == null && isExistingFilter) {
    return null;
  }
  return (
    <Page
      headerTitle="Zoeken"
      navigationSelectedItem="Dashboard"
      headerBackButtonAction={() => {
        history.goBack();
      }}
    >
      <GridLayout templateColumns={6}>
        <GridLayoutCell columnSpan={6}>
          <Heading level={3}>{filterPageTitle}</Heading>
        </GridLayoutCell>

        {(!(isNewFilter || isExistingFilter) || searchFilter?.id !== '_unsavedFilters') && _nameField}
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Ik zoek als:</Heading>
          <FormSelectionButtons
            initialValue={searchFilter?.entity}
            options={[
              { value: 'individual', label: 'Particulier' },
              { value: 'organization', label: 'Organisatie' },
            ]}
            onValueChange={(value) => {
              updateSearchFilterField('entity', value as SearchFilter['entity']);
            }}
          />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Woonsituatie:</Heading>
          <FormSelectionButtons
            initialValue={searchFilter?.livingSituation}
            options={[
              { value: 'living with parents', label: 'Thuiswonend' },
              { value: 'owner-occupied house', label: 'Koophuis' },
              { value: 'rental house', label: 'Huurwoning' },
              { value: 'student residence', label: 'Studentenwoning' },
              { value: 'something else', label: 'Iets anders' },
            ]}
            onValueChange={(value) => {
              updateSearchFilterField('livingSituation', value as SearchFilter['livingSituation']);
            }}
          />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Werksituatie:</Heading>
          <FormSelectionButtons
            initialValue={searchFilter?.workSituation}
            options={[
              { value: 'student', label: 'Student' },
              { value: 'looking for work', label: 'Werkzoekende' },
              { value: 'full-time job', label: 'Full-time baan' },
              { value: 'part-time job', label: 'Part-time baan' },
              { value: 'volunteer work', label: 'Vrijwilligerswerk' },
              { value: 'unemployed', label: 'Werkloos' },
            ]}
            onValueChange={(value) => {
              updateSearchFilterField('workSituation', value as SearchFilter['workSituation']);
            }}
          />
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Zoeken op thema:</Heading>
          <Select
            value={searchFilter?.themes}
            placeholder="Zoek op thema"
            iconStart={() => Search({})}
            onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
              updateSearchFilterField('themes', [event?.currentTarget?.value]);
            }}
          >
            <SelectOption value="" label="" disabled={false} />
            {themes.map(({ label, icon }: ThemesListItem) => (
              <SelectOption key={label} value={label} label={label} disabled={false} />
            ))}
          </Select>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Zoekenresultaten voor:</Heading>
          <FormField label="Gemeente:" type="textbox">
            <Select
              value={searchFilter?.localAuthorities}
              placeholder="Zoek op gemeente"
              iconEnd={() => ChevronDown({})}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
                updateSearchFilterField('localAuthorities', [event?.currentTarget?.value]);
              }}
            >
              <SelectOption value="" label="" disabled={false} />
              {LocalAuthoritiesData.map((name) => {
                return <SelectOption key={name} value={name} label={name} disabled={false} />;
              })}
            </Select>
          </FormField>
          <FormField label="Provincie:" type="textbox">
            <Select
              value={searchFilter?.provincialAuthorites}
              placeholder="Zoek op provincie"
              iconEnd={() => ChevronDown({})}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
                updateSearchFilterField('provincialAuthorites', [event?.currentTarget?.value]);
              }}
            >
              <SelectOption value="" label="" disabled={false} />
              {ProvincialAuthoritiesData.map((name) => {
                return <SelectOption key={name} value={name} label={name} disabled={false} />;
              })}
            </Select>
          </FormField>
          <FormField label="Waterschappen:" type="textbox">
            <Select
              value={searchFilter?.regionalWaterAuthorites}
              placeholder="Zoek op waterschappen"
              iconEnd={() => ChevronDown({})}
              onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
                updateSearchFilterField('regionalWaterAuthorites', [event?.currentTarget?.value]);
              }}
            />
          </FormField>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <Heading level={5}>Zoektermen toevoegen:</Heading>
          <Select
            placeholder="Type hier..."
            iconStart={() => Search({})}
            value={searchFilter?.searchQuery}
            onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
              updateSearchFilterField('searchQuery', event?.currentTarget?.value);
            }}
          />
        </GridLayoutCell>
        {(!(isNewFilter || isExistingFilter) || searchFilter?.id === '_unsavedFilters') && (
          <GridLayoutCell columnSpan={6}>
            <FormField label="Filters instellen als zoekfilter profiel" type="checkbox" indicatorInfo=".">
              <Checkbox
                className="utrecht-checkbox utrecht-checkbox--html-input"
                required={true}
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                  setAddNewChecked(event?.currentTarget?.checked ?? false);
                }}
              />
            </FormField>
          </GridLayoutCell>
        )}
        {(!(isNewFilter || isExistingFilter) || searchFilter?.id === '_unsavedFilters') && addNewChecked && _nameField}
        <GridLayoutCell columnSpan={6}>
          <ButtonGroup style={{ width: '100%' }}>
            <Button
              style={{ width: '100%' }}
              appearance="primary-action-button"
              onClick={() => {
                const _searchFilter =
                  searchFilter != null
                    ? {
                        ...searchFilter,
                      }
                    : null;
                if (_searchFilter == null) {
                  return;
                }
                if (isNewFilter || (addNewChecked && _searchFilter?.name != null)) {
                  _searchFilter.id = Math.random().toString(16).slice(2);
                } else if (!isExistingFilter) {
                  _searchFilter.id = '_unsavedFilters';
                }
                dispatch(upsertSearchFilter(_searchFilter));
                void dispatch(setActiveFilterAndSearch({ activeFilterId: _searchFilter.id, searchquery: '' }));
                history.push('/search-results');
              }}
            >
              {filterConfirmButton}
            </Button>
            <Button
              style={{ width: '100%' }}
              appearance="secondary-action-button"
              onClick={() => {
                history.push('/search-results');
              }}
            >
              Annuleren
            </Button>
          </ButtonGroup>
        </GridLayoutCell>
      </GridLayout>
    </Page>
  );
}

export default searchFilter;
