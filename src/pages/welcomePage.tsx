import React from 'react';
import { WelcomePage } from '@persoonlijke-regelingen-assistent/components-react';
import { useHistory } from 'react-router';
function Welcome(): ReturnType<React.FC> {
  const history = useHistory();
  return (
    <WelcomePage
      startButtonAction={(e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        history.push('/dashboard');
      }}
    />
  );
}

export default Welcome;
