import React, { type CSSProperties, useEffect } from 'react';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/alternate/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import {
  BreadcrumbNav,
  BreadcrumbNavLink,
  BreadcrumbNavSeparator,
  GridLayout,
  GridLayoutCell,
  Icon,
  MenuItem,
} from '@persoonlijke-regelingen-assistent/components-react';
import './themesOverview.scss';
import { Page } from '../components/Page/Page';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { upsertSearchFilter } from '../state/searchFilters/searchFiltersSlice';
import { fetchThemesList, selectThemesList } from '../state/themesList/themesListSlice';
import nameToIconMapper from '../functions/nameToIconMapper';
import { useHistory } from 'react-router';
import { type ThemesListItem } from '../state/themesList/themesListInterface';

function themesOverview(): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const themes = useAppSelector(selectThemesList) ?? [];
  useEffect(() => {
    void dispatch(fetchThemesList());
  }, []);

  const history = useHistory();

  return (
    <Page
      headerTitle="Dashboard"
      navigationSelectedItem="Dashboard"
      headerSubContent={
        <BreadcrumbNav>
          <BreadcrumbNavLink
            style={{ '--utrecht-icon-inset-block-start': '-0.1em' } satisfies CSSProperties}
            current={false}
            href={`${import.meta.env?.BASE_URL ?? '/'}dashboard`}
            index={0}
          >
            <Icon>{LayoutGrid({})}</Icon>
          </BreadcrumbNavLink>
          <BreadcrumbNavSeparator>
            <Icon>{ChevronRight({})}</Icon>
          </BreadcrumbNavSeparator>

          <BreadcrumbNavLink current={false} href={`${import.meta.env?.BASE_URL ?? '/'}themes-overview`} index={1}>
            Thema&apos;s
          </BreadcrumbNavLink>
        </BreadcrumbNav>
      }
      headerBackButtonAction={() => {
        history.goBack();
      }}
    >
      <GridLayout templateColumns={6}>
        <GridLayoutCell columnSpan={6}>
          <div
            style={
              {
                display: 'flex',
                'flex-direction': 'column',
                'align-items': 'start',
                'justify-content': 'center',
                gap: '24px',
              } satisfies CSSProperties
            }
          >
            {themes.map((themesListItem: ThemesListItem) => {
              const { label, icon } = themesListItem;
              const _icon = nameToIconMapper(icon);
              return (
                <MenuItem
                  key={label}
                  icon={_icon != null ? _icon({}) : undefined}
                  label={label}
                  mode="horizontal"
                  onClick={() => {
                    dispatch(
                      upsertSearchFilter({
                        ...{ id: '_unsavedFilters' },
                        themes: [label],
                      }),
                    );
                    history.push('/search-results');
                  }}
                />
              );
            })}
          </div>
        </GridLayoutCell>
      </GridLayout>
    </Page>
  );
}

export default themesOverview;
