import AlertTriangle from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/AlertTriangle';
import React, { useEffect } from 'react';
import {
  DonutChartWithDetails,
  InformationAlert,
  LoadingPage,
  RegulationPage,
} from '@persoonlijke-regelingen-assistent/components-react';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { selectItem, fetch } from '../state/items/itemsSlice';
import { calculateTax, selectVariables } from '../state/modelExecutor/modelExecutorSlice';

import { useHistory } from 'react-router';
import { Amount, Content, type Item, type Regulation } from '../state/items/itemsInterface';
import htmlContentBlockRenderer from '../functions/htmlContentBlockRenderer';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';
import { saveOrRemoveItem, isItemSaved } from '../state/savedItems/savedItemsSlice';
import nameToImageMapper from '../functions/nameToImageMapper';
import { selectProfile } from '../state/profile/profileSlice';

function regulation({ id }: { id: string }): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const history = useHistory();

  useEffect(() => {
    void dispatch(fetch(Number(id)));
  }, []);

  const baseURL = import.meta.env?.BASE_URL ?? '/';
  const isBookmarked = useAppSelector(isItemSaved({ id: Number(id), type: 'regulation' }));
  const isRegulation = (item: Item | undefined): item is Regulation =>
    item != null && Object.keys(item).includes('Regeling.Id');
  const item = useAppSelector(selectItem(Number(id)));
  const calculationVariables = useAppSelector(selectVariables);
  const profileData = useAppSelector(selectProfile);
  if (!isRegulation(item)) {
    return null;
  } else {
    const regulation = item;

    if (regulation?.Id === 5596916971521 && profileData.pension == null) {
      history.push(`/settings/wallet`);
    } else if (
      regulation?.Id === 5596916971521 &&
      profileData.pension != null &&
      calculationVariables?.['aow-belasting/inkomstenbelasting_aow/Geboortedatum'] == null
    ) {
      void dispatch(
        calculateTax({
          birthDate: profileData.birthdate || '',
          pension: profileData.pension,
          aow: profileData.aow || 0,
        }),
      );
      return <LoadingPage />;
    } else {
      const replaceAmountVariables = (amountFields: Amount) => {
        let newAmountFields = structuredClone(amountFields);
        if (newAmountFields.Parts != null) {
          if (typeof newAmountFields.Parts.Labels === 'string') {
            newAmountFields.Parts.Labels = calculationVariables[newAmountFields.Parts.Labels] as string[];
          }
          if (typeof newAmountFields.Parts.Values === 'string') {
            newAmountFields.Parts.Values = calculationVariables[newAmountFields.Parts.Values] as number[];
          }
        }
        if (amountFields.Amount != null && typeof amountFields.Amount === 'string') {
          newAmountFields.Amount = (calculationVariables[amountFields.Amount] as number) || 0;
        }
        return newAmountFields;
      };

      const contentBlockMapper = ({ ContentBlocks = [], ContentType, ContentId, Label }: Content) => {
        const ContentElements = ContentBlocks.map(({ ContentBlockType, Body, Calculations, HeaderAmount }) => {
          if (ContentBlockType === 'HtmlContentBlock') {
            return htmlContentBlockRenderer(Body);
          } else if (ContentBlockType === 'DonutChartWithDetails') {
            Calculations = Calculations.map((Calculation) => {
              let { Amounts, TotalAmount, ResultingAmount, ...CalculationProps } = Calculation;
              if (Amounts) {
                Amounts = Amounts.map(replaceAmountVariables);
              }
              if (TotalAmount) {
                TotalAmount = replaceAmountVariables(TotalAmount);
              }
              if (ResultingAmount) {
                ResultingAmount = replaceAmountVariables(ResultingAmount);
              }
              return { Amounts, TotalAmount, ResultingAmount, ...CalculationProps };
            });
            HeaderAmount = replaceAmountVariables(HeaderAmount);
            return <DonutChartWithDetails Calculations={Calculations} HeaderAmount={HeaderAmount} />;
          } else if (ContentBlockType === 'AlertContentBlock') {
            return (
              <InformationAlert icon={AlertTriangle} title="Let op">
                {htmlContentBlockRenderer(Body)}
              </InformationAlert>
            );
          }
          return null;
        });
        return { ContentType, ContentId, Label, ContentElements };
      };

      const content = regulation?.['Regeling.Content']?.map(contentBlockMapper);

      const id = regulation?.Id;

      const uitlegFunctionaliteit = regulation?.['Regeling.Functionaliteiten']?.find(
        (f: { Functionaliteit: string }) => f?.Functionaliteit === 'Uitleggen',
      );
      const title = uitlegFunctionaliteit?.Titel ?? regulation?.['Zoekdescriptor.Titel'];
      const description = uitlegFunctionaliteit?.Intro ?? regulation?.['Zoekdescriptor.Korte_Omschrijving'];
      const minutesIndication = regulation?.['Zoekdescriptor.LeestijdMinuten'] ?? 0;
      const authority = regulation?.['Wordt_Onderhouden_Door'] ?? undefined;
      const speachBulbBody = regulation?.DefaultSpeachBulb?.Body;
      const speachBulbHyperLinkLabel = regulation?.DefaultSpeachBulb?.HyperlinkText;
      const speachBulbTitle = regulation?.DefaultSpeachBulb?.Title;
      const speachBulbHyperlinkClickHandler = () => {
        history.push(
          `${baseURL}${regulation?.DefaultSpeachBulb?.HyperlinkRef}`,
        );
      };

      const imgSrc = nameToImageMapper[`${regulation.MainImage}`];

      return (
        <RegulationPage
          navigationBar={<MenuNavigation selectedItem="Dashboard" indicators={[]} />}
          baseURL={baseURL}
          indicator={{
            showIndicator: minutesIndication > 0,
            bookmarkButtonAction: () => {
              dispatch(
                saveOrRemoveItem({
                  id,
                  type: 'regulation',
                  title,
                  message: description,
                }),
              );
            },
            isBookmarked,
          }}
          speachBulb={{
            body: speachBulbBody,
            hyperlinkLabel: speachBulbHyperLinkLabel,
            hyperlinkClickHandler: speachBulbHyperlinkClickHandler,
            title: speachBulbTitle,
          }}
          regulation={{
            title,
            id,
            authority,
            description: htmlContentBlockRenderer(description),
            minutesIndication,
            content,
            imgSrc,
          }}
        />
      );
    }
  }
}

export default regulation;
