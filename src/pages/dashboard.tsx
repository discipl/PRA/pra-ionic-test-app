import React, { useEffect } from 'react';
import { DashboardPage } from '@persoonlijke-regelingen-assistent/components-react';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { search } from '../state/searchResults/searchResultsSlice';
import { useHistory } from 'react-router';
import { fetchThemesList, selectThemesList } from '../state/themesList/themesListSlice';
import { upsertSearchFilter } from '../state/searchFilters/searchFiltersSlice';
import { fetchLifeEventsList, selectLifeEventsList } from '../state/lifeEventsList/lifeEventsListSlice';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';

function Dashboard(): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const themesList = useAppSelector(selectThemesList) ?? [];
  const lifeEventsList = useAppSelector(selectLifeEventsList) ?? [];

  const generateGreeting = (): string => {
    const now = new Date();
    const hours = now.getHours();

    if (hours < 12) {
      return 'Goedemorgen';
    } else if (hours < 18) {
      return 'Goedemiddag';
    } else {
      return 'Goedenavond';
    }
  };

  const onSearchBarEnterKey = async (searchQuery: string): Promise<void> => {
    await dispatch(search({ searchquery: searchQuery }));
    history.push('/search-results');
  };

  const greeting = generateGreeting();
  const baseURL = import.meta.env?.BASE_URL ?? '/';

  useEffect(() => {
    void dispatch(fetchThemesList());
    void dispatch(fetchLifeEventsList());
  }, []);

  return (
    <DashboardPage
      navigationBar={<MenuNavigation />}
      greeting={greeting}
      onSearchBarEnterKey={onSearchBarEnterKey}
      themesList={themesList}
      lifeEventsList={lifeEventsList}
      baseURL={baseURL}
      onThemeClick={({ label, icon }: { label: string; icon?: JSX.Element }) => {
        dispatch(
          upsertSearchFilter({
            ...{ id: '_unsavedFilters' },
            themes: [label],
          }),
        );
        history.push('/search-results');
      }}
    />
  );
}

export default Dashboard;
