import React, { useEffect } from 'react';
import './task.scss';
import { Textbox } from '@persoonlijke-regelingen-assistent/components-react';
import { updateTask, selectTask } from '../state/tasks/tasksSlice';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { Page } from '../components/Page/Page';
export interface TaskProps {
  id: string;
}
function Tasks(props: TaskProps): ReturnType<React.FC<TaskProps>> {
  const { id } = props;
  const task = useAppSelector(selectTask(id));
  const dispatch = useAppDispatch();

  const dateToISOLocal = (d: Date): string => {
    d.setTime(d.getTime() - d.getTimezoneOffset() * 60000);
    return d.toISOString().replace(/:\d+.\d+Z$/g, '');
  };
  const localNumberDateToUTCDate = (numberValue: number): Date => {
    const dt = new Date(numberValue);
    dt.setMinutes(dt.getMinutes() + dt.getTimezoneOffset());
    return dt;
  };

  return (
    <Page headerTitle="Taak" navigationSelectedItem="Tasks">
      <div className="task">
        <h1>Taak {id}</h1>
        <Textbox
          invalid={false}
          type="text"
          min={0}
          required={true}
          value={task?.title ?? ''}
          onChange={(e) => {
            dispatch(updateTask({ id, task: { ...task, title: e.target.value } }));
          }}
        />
        <br />
        <Textbox
          invalid={false}
          type="text"
          min={0}
          required={true}
          value={task?.description ?? ''}
          onChange={(e) => {
            dispatch(updateTask({ id, task: { ...task, description: e.target.value } }));
          }}
        />
        <br />
        Afgerond: <br />
        <p
          onClick={() => {
            dispatch(
              updateTask({
                id,
                task: { ...task, completed: !(task?.completed ?? false) },
              }),
            );
          }}
        >
          {task?.completed ?? false ? 'True' : 'False'}
        </p>
        <br />
        Completed:
        <input
          type="datetime-local"
          value={task?.completedTimestamp}
          onChange={(e) => {
            dispatch(
              updateTask({
                id,
                task: { ...task, completedTimestamp: e.target.value },
              }),
            );
          }}
        />
        <br />
        Created:
        <input
          type="datetime-local"
          value={task?.createdTimestamp}
          onChange={(e) => {
            dispatch(
              updateTask({
                id,
                task: { ...task, createdTimestamp: e.target.value },
              }),
            );
          }}
        />
        Notification:
        <input
          type="datetime-local"
          value={task?.notificaitonTimestamp != null ? dateToISOLocal(task?.notificaitonTimestamp) : ''}
          onChange={(e) => {
            dispatch(
              updateTask({
                id,
                task: {
                  ...task,
                  notificaitonTimestamp: localNumberDateToUTCDate(e.target.valueAsNumber),
                },
              }),
            );
          }}
        />
      </div>
    </Page>
  );
}

export default Tasks;
