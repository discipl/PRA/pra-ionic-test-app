import React, { useEffect, type CSSProperties } from 'react';
import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';
import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import LayoutGrid from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/LayoutGrid';
import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import {
  BreadcrumbNav,
  BreadcrumbNavLink,
  BreadcrumbNavSeparator,
  GridLayout,
  GridLayoutCell,
  Heading,
  Icon,
  MenuItem,
  Paragraph,
} from '@persoonlijke-regelingen-assistent/components-react';
import { Page } from '../components/Page/Page';
import { useHistory } from 'react-router';
import { fetchLifeEventsList, selectLifeEventsList } from '../state/lifeEventsList/lifeEventsListSlice';
import { useAppDispatch, useAppSelector } from '../state/hooks';
import { type LifeEventsListItem } from '../state/lifeEventsList/lifeEventsListInterface';

function LifeEventsOverview(): ReturnType<React.FC> {
  const dispatch = useAppDispatch();
  const lifeEventsList = useAppSelector(selectLifeEventsList) ?? [];
  useEffect(() => {
    void dispatch(fetchLifeEventsList());
  }, []);

  const history = useHistory();
  return (
    <Page
      headerTitle="Dashboard"
      navigationSelectedItem="Dashboard"
      headerBackButtonAction={() => {
        history.goBack();
      }}
      headerSubContent={
        <BreadcrumbNav>
          <BreadcrumbNavLink
            style={
              {
                '--utrecht-icon-inset-block-start': '-0.1em',
              } satisfies CSSProperties
            }
            current={false}
            href={`${import.meta.env?.BASE_URL ?? '/'}dashboard`}
            index={0}
          >
            <Icon>{LayoutGrid({})}</Icon>
          </BreadcrumbNavLink>
          <BreadcrumbNavSeparator>
            <Icon>{ChevronRight({})}</Icon>
          </BreadcrumbNavSeparator>
          <BreadcrumbNavLink current={false} href={`${import.meta.env?.BASE_URL ?? '/'}life-events`} index={1}>
            Levensgebeurtenissen
          </BreadcrumbNavLink>
        </BreadcrumbNav>
      }
    >
      <GridLayout templateColumns={6}>
        <GridLayoutCell columnSpan={6}>
          <Heading level={1}>Levensgebeurtenissen</Heading>
          <Paragraph>
            De overheid maakt persoonlijke overzichten voor levensgebeurtenissen zoals kind krijgen, studeren en
            overlijden. Alles wat je moet regelen met de overheid of waar je recht op hebt is te vinden in 1 persoonlijk
            overzicht. Hieronder vindt u alle persoonlijke overzichten.
          </Paragraph>
        </GridLayoutCell>
        <GridLayoutCell columnSpan={6}>
          <div
            style={
              {
                display: 'flex',
                'flex-direction': 'column',
                'align-items': 'start',
                'justify-content': 'center',
                gap: '24px',
                paddingBlockEnd: '16px',
              } satisfies CSSProperties
            }
          >
            {lifeEventsList.map((lifeEventListItem: LifeEventsListItem) => {
              const { name, id } = lifeEventListItem;
              return (
                <MenuItem
                  key={name}
                  imageSrc={birthday_party}
                  label={name}
                  mode="horizontal"
                  onClick={() => {
                    history.push(`/life-events/${id}`);
                  }}
                />
              );
            })}
          </div>
        </GridLayoutCell>
      </GridLayout>
    </Page>
  );
}

export default LifeEventsOverview;
