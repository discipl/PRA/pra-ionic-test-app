import React from 'react';
import { SettingsMenuPage } from '@persoonlijke-regelingen-assistent/components-react';
import { useHistory } from 'react-router';
import MenuNavigation from '../components/MenuNavigation/MenuNavigation';

function Info(): ReturnType<React.FC> {
  const history = useHistory();
  return (
    <SettingsMenuPage
      menuItems={[
        {
          label: 'Persoonlijke gegevens',
          action: () => {
            history.push('/settings/profile');
          },
        },
        {
          label: 'Meldingen',
          action: () => {
            history.push('/settings/tasks');
          },
        },
        { label: 'Toegankelijkheid', action: () => {} },
        {
          label: 'Over de PRA',
          action: () => {
            history.push('/info');
          },
        },
        { label: 'Beveiliging en privacy', action: () => {} },
        {
          label: 'Wallet gegevens',
          action: () => {
            history.push('/settings/wallet');
          },
        },
        { label: 'Tips en support', action: () => {} },
      ]}
      menuTitle="Instellingen"
      navigationBar={<MenuNavigation selectedItem="Instellingen" indicators={[]} />}
    />
  );
}

export default Info;
