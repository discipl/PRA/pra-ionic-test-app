import React, { type ReactElement, type FC, type ReactNode, type Attributes, useEffect } from 'react';
import { IonReactRouter } from '@ionic/react-router';
import { Route, useParams, Switch, Redirect, useLocation } from 'react-router-dom';
import ThemesOverview from '../pages/themesOverview';
import Dashboard from '../pages/dashboard';
import SearchFilter from '../pages/searchFilter';
import SearchResults from '../pages/searchResults';
import Tasks from '../pages/tasks';
import Info from '../pages/info';
import Task from '../pages/task';
import LifeEventsOverview from '../pages/lifeEventsOverview';
import AppListener from '../components/AppListener/AppListener';

import { useAppSelector } from '../state/hooks';
import { selectAppState } from '../state/app/appSlice';
import Welcome from '../pages/welcomePage';
import SavedItems from '../pages/savedItems';
import Regulation from '../pages/regulation';
import LifeEvent from '../pages/lifeEvent';
import Profile from '../pages/profile';
import SettingsMenu from '../pages/settingsMenu';
import Wallet from '../pages/wallet';
import { ensurePermissions } from '../functions/localNotifications';
/**
 * The component that contains the routes in the application
 */
const TaskPage = (): ReactElement => {
  const { taskId } = useParams<{ taskId: string }>();
  return <Task id={taskId} />;
};
const RegulationPage = (): ReactElement => {
  const { regulationId } = useParams<{ regulationId: string }>();
  return <Regulation id={regulationId} />;
};
const LifeEventPage = (): ReactElement => {
  const { lifeEventId } = useParams<{ lifeEventId: string }>();
  return <LifeEvent id={lifeEventId} />;
};
const WalletPage = (): ReactElement => {
  const { search } = useLocation();
  const searchParams = new URLSearchParams(search);
  const responseCode = searchParams.get('wallet_response_code') ?? undefined;
  return <Wallet walletResponseCode={responseCode} />;
};
const SearchFilterWithFilterId = (): ReactElement => {
  const { filterId } = useParams<{ filterId: string }>();
  return <SearchFilter filterId={filterId} />;
};

export type PRARoute = Record<
  string,
  {
    path: string;
    pageComponent: ReactNode;
  }
>;

const CustomRoute = ({ component, path, key }: { component: FC; path: string; key: string }): ReactElement => {
  const appState = useAppSelector(selectAppState);
  const isAuthenticated = appState.firstUse === false;

  if (!isAuthenticated) {
    return <Route key={key} path={path} render={() => <Redirect to={{ pathname: '/info' }} />} />;
  }
  return <Route key={key} path={path} component={(props: Attributes) => React.createElement(component, props)} />;
};

const Router: React.FC = () => {
  useEffect(() => {
    void ensurePermissions();
  }, []);
  return (
    <IonReactRouter basename={import.meta.env?.BASE_URL ?? '/'}>
      <AppListener></AppListener>
      <Switch>
        <CustomRoute key={`route-settings-profile`} path="/settings/profile" component={() => <Profile />} />
        <CustomRoute
          key={`route-settings-tasks/:taskId`}
          path="/settings/tasks/:taskId"
          component={() => <TaskPage />}
        />
        <CustomRoute key={`route-settings-tasks`} path="/settings/tasks" component={() => <Tasks />} />
        <CustomRoute key={`route-settings-wallet`} path="/settings/wallet" component={() => <WalletPage />} />
        <CustomRoute key={`route-settings`} path="/settings" component={() => <SettingsMenu />} />
        <CustomRoute key={`route-saved-items`} path="/saved-items" component={() => <SavedItems />} />
        <CustomRoute key={`route-regulation`} path="/regulations/:regulationId" component={() => <RegulationPage />} />
        <CustomRoute key={`route-life-event`} path="/life-events/:lifeEventId" component={() => <LifeEventPage />} />
        <CustomRoute key={`route-life-events`} path="/life-events" component={() => <LifeEventsOverview />} />
        <CustomRoute key={`route-search-results`} path="/search-results" component={() => <SearchResults />} />

        <CustomRoute
          key={`route-search-filter/:filterId`}
          path="/search-filter/:filterId"
          component={() => <SearchFilterWithFilterId />}
        />
        <CustomRoute key={`route-search-filter`} path="/search-filter" component={() => <SearchFilter />} />
        <CustomRoute key={`route-dashboard`} path="/dashboard" component={() => <Dashboard />} />
        <CustomRoute key={`themes-overview`} path="/themes-overview" component={() => <ThemesOverview />} />
        <Route key={`route-info`} path="/info" component={() => <Info />} />
        <CustomRoute key={`route-/`} path="/" component={() => <Welcome />} />
        <CustomRoute key={`route-*`} path="*" component={() => <Welcome />} />
      </Switch>
    </IonReactRouter>
  );
};

export default Router;
