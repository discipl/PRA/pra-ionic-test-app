import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState } from '../store';

import fetchSearchTerms, {
  type SearchTermsRequestBody,
  type SearchTermsResponse,
} from '../../endpoints/fetchSearchTerms';
import { type SearchTerms } from './searchTermsInterface';

/**
 * The initial state for the SearchTerms slice
 */
const initialState: SearchTerms = {};

/**
 * Selector for getting the Ministries;
 */
export const selectMinistries = (state: RootState): string[] | undefined => state.searchTerms.ministeries;
/**
 * Selector for getting the Subjects;
 */
export const selectSubjects = (state: RootState): string[] | undefined => state.searchTerms.subjects;

/**
 * Thunk function for fetching the SearchTerms
 */
export const fetch = createAsyncThunk(
  'searchTerms/fetch',
  async ({ subjects, ministeries }: SearchTermsRequestBody): Promise<SearchTermsResponse> => {
    return await fetchSearchTerms({
      subjects,
      ministeries,
    });
  },
);

/**
 * The searchTermsSlice in the redux state
 */
export const SearchTermsSlice = createSlice({
  name: 'searchTerms',
  initialState,
  extraReducers: (builder) => {
    /**
     * Add the SearchTerms to the state when the fetch request is completed
     */
    builder.addCase(fetch.fulfilled, (state, action) => {
      const searchTermsResponse = action.payload;
      return processSearchTermsResponse(state, searchTermsResponse);
    });
  },
  reducers: {},
});

const processSearchTermsResponse = (state: SearchTerms, searchTermsResponse: SearchTermsResponse): SearchTerms => {
  const { subjects, ministeries } = searchTermsResponse;
  return {
    subjects,
    ministeries,
  };
};
export default SearchTermsSlice.reducer;
