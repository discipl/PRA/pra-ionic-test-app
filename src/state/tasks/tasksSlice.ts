import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';

import { type Task } from './tasksInterfaces';
import { scheduleNotification } from '../../functions/localNotifications';

/**
 * The initial state for the Tasks slice
 */
const initialState: Task[] = [];

/**
 * Selector for getting the Task;
 */
export const selectTasks = (state: RootState): Task[] => state.tasks;
export const selectTask = (id: string) => (state: RootState) => state.tasks[state.tasks.findIndex((x) => x.id === id)];

const generateId = (): number => Math.floor(Math.random() * 10);

/**
 * The TasksSlice in the redux state
 */
export const TasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    addTask: (state) => {
      const id = state.length + '';
      const title = 'Kinderbijslag';
      const description = 'SVB';
      if (id.length > 0) {
        state.push({ id, title, description });
      }
    },
    updateTask: (
      state,
      action: PayloadAction<{
        id: string;
        task: Task;
      }>,
    ) => {
      const { id, task } = action.payload;
      if (id.length > 0) {
        const foundIndex = state.findIndex((x) => x.id === id);
        state[foundIndex] = {
          ...state[foundIndex],
          ...task,
        };
      }
      if (task.notificaitonTimestamp != null) {
        const dateTime = new Date(task.notificaitonTimestamp.getTime());
        void scheduleNotification({
          title: `Taak ${task.title}`,
          body: `${task.description}`,
          extra: {
            taskId: task.id,
          },
          schedule: { at: dateTime },
        });
      }
    },
    markCompleted: (
      state,
      action: PayloadAction<{
        id: string;
      }>,
    ) => {
      const { id } = action.payload;
      const dateString = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
      if (id.length > 0) {
        const foundIndex = state.findIndex((x) => x.id === id);
        state[foundIndex] = {
          ...state[foundIndex],
          completed: true,
          completedTimestamp: dateString,
        };
      }
    },
  },
});
export const { addTask, updateTask, markCompleted } = TasksSlice.actions;

export default TasksSlice.reducer;
