export interface Task {
  id: string;
  title?: string;
  description?: string;
  completed?: boolean;
  notificaitonTimestamp?: Date;
  createdTimestamp?: string;
  completedTimestamp?: string;
}
