export type ModelResultValue = string | boolean | number | string[] | boolean[] | number[];

export interface ExecutorSlice {
  results: Record<string, ModelResultValue>;
  executing: Record<string, boolean>;
  isStarted: boolean;
}
