import { createAsyncThunk, createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';
import { ExecutorSlice, ModelResultValue } from './modelExecutorInterface';
import { execute, startModelExecutor } from '../../endpoints/executeModels';
import { scheduleNotificationIn10Seconds } from '../../functions/localNotifications';

/**
 * The initial state for the Items slice
 */
const initialState: ExecutorSlice = { results: {}, executing: {}, isStarted: false };

/**
 * Selector for getting the ProfileState;
 */
export const selectVariables = (state: RootState): Record<string, ModelResultValue> => state.modelExecutor.results;

export const selectModelIsExecuting =
  (id: number) =>
  (state: RootState): boolean =>
    state.modelExecutor.executing[`${id}`];

/**
 * Thunk function for calculating the tax
 */
export const calculateTax = createAsyncThunk(
  'items/calculateTax',
  async (
    {
      birthDate,
      pension,
      aow,
    }: {
      birthDate: string;
      pension: Record<string, number>;
      aow: number;
    },
    { dispatch },
  ): Promise<any> => {
    const model = 'aow-belasting';
    const functionality = 'inkomstenbelasting_aow';
    const Geboortedatum = birthDate;
    const Kalenderjaar = new Date().getFullYear();
    const Alleenstaand = false;
    const PotjesBedragen = Object.values(pension || {})
      .map(String)
      .join(' ');
    const AowBedrag = aow;
    const inputs = {
      Geboortedatum: '1957-01-01', //TODO
      Kalenderjaar,
      Alleenstaand,
      AowBedrag,
      PotjesBedragen,
    };

    await dispatch(upsertVariables({ 'persoonsgegevens/Pensioenfondsen': Object.keys(pension || {}) }));

    await dispatch(start());
    return await execute(model, functionality, inputs);
  },
);

/**
 * Thunk function for calculating the pension date
 */
export const calculatePensionDate = createAsyncThunk(
  'items/calculatePensionDate',
  async ({ birthDate }: { birthDate: string }, { dispatch, getState }): Promise<any> => {
    const id = 1036374822298741800;
    await dispatch(start());
    let currentDate = new Date();
    const today = currentDate.toISOString().split('T')[0];
    const model = 'algemene-ouderdomswet';
    const functionality = 'notificatie';
    const inputs = {
      Geboortedatum: {
        value: birthDate,
        source: 'wallet_dateOfBirthOnPID',
        reason: 'nodig om te bepalen of de gebruiker een notificatie moet ontvangen',
      },
      Vandaag: {
        value: today,
        source: 'clockOnUserDevice',
        reason: 'nodig om te bepalen of de nodtificatie op dit moment relevant is',
      },
    };
    const result = await execute(model, functionality, inputs);
    return result;
  },
);

/**
 * The CalculationVariablesSlice in the redux state
 */
export const CalculationVariablesSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    start: (state) => {
      if (!state.isStarted) {
        startModelExecutor();
        state.isStarted = true;
      }
    },
    upsertVariable: (
      state,
      action: PayloadAction<{ key: keyof Record<string, ModelResultValue>; value: ModelResultValue }>,
    ) => {
      const { key, value } = action.payload;
      state.results[key] = value;
      return state;
    },
    upsertVariables: (state, action: PayloadAction<Record<string, ModelResultValue>>) => {
      return { ...state, ...action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(calculateTax.fulfilled, (state, action) => {
      const fullNameVariables = action.payload;
      return { ...state, results: { ...state.results, ...fullNameVariables } };
    });
    builder.addCase(calculatePensionDate.fulfilled, (state, action) => {
      const name = 'Mark'; //TODO profiledata.givenname;
      scheduleNotificationIn10Seconds({
        title: 'PRA - Pensioen',
        body: `${name}, over ${action.payload} dagen bereik je de pensioengerechtigde leeftijd. Check wat je moet regelen in de PRA app.`,
        extra: { regulationId: '5596916971520' },
      });

      return { ...state, results: { ...state.results, 'algemene-ouderdomswet/dagenTotPensioen': action.payload } };
    });
  },
});
export const { upsertVariable, upsertVariables, start } = CalculationVariablesSlice.actions;

export default CalculationVariablesSlice.reducer;
