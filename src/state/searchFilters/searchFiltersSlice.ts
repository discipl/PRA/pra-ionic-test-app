import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';
import { type SearchFilter } from './searchFiltersInterface';

/**
 * The initial state for the SearchFilters slice
 */
const initialState: SearchFilter[] = [];

/**
 * Selector for getting the SearchFilters;
 */
export const selectSearchFilter = (filterId: string) => (state: RootState) =>
  state.searchFilters.find(({ id }) => id === filterId);
export const selectSearchFilterNames = (
  state: RootState,
): Array<{ id: string | undefined; name: string | undefined }> =>
  state.searchFilters.map(({ id, name }) => ({ id, name }));

/**
 * The SearchFiltersSlice in the redux state
 */
export const SearchFiltersSlice = createSlice({
  name: 'searchFilters',
  initialState,
  reducers: {
    upsertSearchFilter: (state, action: PayloadAction<SearchFilter>) => {
      const searchFilter = action.payload;
      if (
        (searchFilter?.name?.length == null || searchFilter?.name?.length === 0) &&
        searchFilter?.id !== '_unsavedFilters'
      ) {
        return;
      }
      const index = state.findIndex((_searchFilter) => _searchFilter.id === searchFilter.id);
      if (index >= 0) {
        state[index] = searchFilter;
      } else {
        state.push(searchFilter);
      }
    },
  },
});
export const { upsertSearchFilter } = SearchFiltersSlice.actions;
export default SearchFiltersSlice.reducer;
