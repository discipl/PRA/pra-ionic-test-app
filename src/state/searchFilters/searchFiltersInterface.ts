export interface SearchFilter {
  id?: string;
  name?: string;
  entity?: 'individual' | 'organization';
  livingSituation?:
    | 'living with parents'
    | 'owner-occupied house'
    | 'rental house'
    | 'student residence'
    | 'something else';
  workSituation?: 'student' | 'looking for work' | 'full-time job' | 'part-time job' | 'volunteer work' | 'unemployed';
  themes?: string[];
  localAuthorities?: string[];
  provincialAuthorites?: string[];
  regionalWaterAuthorites?: string[];
  searchQuery?: string;
}
