import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';
import { type App } from './appInterface';

/**
 * The initial state for the App slice
 */
const initialState: App = {};

/**
 * Selector for getting the AppState;
 */
export const selectAppState = (state: RootState): App => state.app;

/**
 * The AppSlice in the redux state
 */
export const AppSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setFirstUse: (state, action: PayloadAction<boolean>) => {
      state.firstUse = action.payload;
    },
  },
});
export const { setFirstUse } = AppSlice.actions;

export default AppSlice.reducer;
