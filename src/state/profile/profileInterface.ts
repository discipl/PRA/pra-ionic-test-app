export interface Profile {
  givenname?: string;
  birthdate?: string;
  familyname?: string;
  aow?: number;
  pension?: Record<string, number>;
  postalcode?: string;
  tvdistinction?: 'T' | 'V';
  avatar?: string;
}
