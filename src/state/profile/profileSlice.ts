import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';
import { type Profile } from './profileInterface';

/**
 * The initial state for the Profile slice
 */
const initialState: Profile = {};

/**
 * Selector for getting the ProfileState;
 */
export const selectProfile = (state: RootState): Profile => state.profile;

/**
 * The ProfileSlice in the redux state
 */
export const ProfileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    upsertProfileData: (state, action: PayloadAction<Profile>) => {
      return { ...state, ...action.payload };
    },
    upsertProfileDataValue: (
      state,
      action: PayloadAction<{ key: keyof Profile; value: string | boolean | undefined }>,
    ) => {
      const { key, value } = action.payload;
      if (key === 'givenname' && typeof value === 'string') {
        state.givenname = value;
      } else if (key === 'birthdate' && typeof value === 'string') {
        state.birthdate = value;
      } else if (key === 'postalcode' && typeof value === 'string') {
        state.postalcode = value;
      } else if ((key === 'tvdistinction' && value === 'T') || value === 'V') {
        state.tvdistinction = value;
      } else if (key === 'avatar' && typeof value === 'string') {
        state.avatar = value;
      }
    },
  },
});
export const { upsertProfileData, upsertProfileDataValue } = ProfileSlice.actions;

export default ProfileSlice.reducer;
