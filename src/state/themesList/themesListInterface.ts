export interface ThemesListItem {
  label: string;
  icon: string;
}
