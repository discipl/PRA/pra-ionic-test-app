import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState } from '../store';

import fetchCategories, { type CategoriesResponseItem, type CategoriesResponse } from '../../endpoints/fetchCategories';
import { type ThemesListItem } from './themesListInterface';

/**
 * The initial state for the ThmesList slice
 */
const initialState: ThemesListItem[] = [];

/**
 * Selector for getting the ThmesList;
 */
export const selectThemesList = (state: RootState): ThemesListItem[] | undefined => state.themesList;

/**
 * Thunk function for fetching the ThmesList
 */
export const fetchThemesList = createAsyncThunk('themesList/fetch', async (): Promise<CategoriesResponse> => {
  return await fetchCategories({
    kind: 'Thema',
    language: 'nl-NL',
  });
});

/**
 * The ThemesListSlice in the redux state
 */
export const ThemesListSlice = createSlice({
  name: 'themesList',
  initialState,
  extraReducers: (builder) => {
    /**
     * Add the ThmesList to the state when the fetch request is completed
     */
    builder.addCase(fetchThemesList.fulfilled, (state, action) => {
      const categoriesResponse = action.payload;
      return processCategoriesResponse(state, categoriesResponse);
    });
  },
  reducers: {},
});

const processCategoriesResponse = (
  state: ThemesListItem[],
  categoriesResponse: CategoriesResponse,
): ThemesListItem[] => {
  const themesList: ThemesListItem[] = [];
  categoriesResponse?.results?.forEach((category: CategoriesResponseItem) => {
    let icon = '';
    let label = '';
    icon = category['Categorie.Icon'];
    label = category['Categorie.Titel'];

    themesList.push({ icon, label });
  });

  return themesList;
};
export default ThemesListSlice.reducer;
