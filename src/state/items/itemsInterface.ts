export type ContentBlockType = 'HtmlContentBlock';
export type Functionaliteit = 'Uitleggen';
export interface Amount {
  Label: string;
  AmountType: 'Total' | 'Single' | 'Unknown';
  Color?: string;
  Amount?: string | number;
  Parts?: {
    Values: number[] | string;
    Labels: string[] | string;
  };
}
export interface Calculation {
  Label: string;
  CalculationType: 'Summation' | 'Substraction';
  Amounts?: Amount[];
  TotalAmount?: Amount;
  ResultingAmount?: Amount;
}
export interface ContentBlock {
  ContentBlockType: ContentBlockType;
  ContentBlockId: number;
  AnchorLink: string;
  Body: string;
  Calculations: Calculation[];
  HeaderAmount: Amount;
}
export interface ItemBaseProps {
  Compleet: boolean;
  Id: number;
  Wordt_Onderhouden_Door: string;
  Type: string;
  'Zoekdescriptor.Laatst_Bewerkt_Op': string;
  'Zoekdescriptor.Taal': string;
  'Zoekdescriptor.Variant': string;
  'Zoekdescriptor.Model': string;
  'Zoekdescriptor.Titel': string;
  'Zoekdescriptor.Korte_Omschrijving': string;
  'Zoekdescriptor.Filters': string[];
  'Zoekdescriptor.Zoektermen': string[];
  'Zoekdescriptor.LeestijdMinuten': number;
}

export interface Content {
  Label: string;
  Group: string;
  ContentType: 'Plain' | 'Expandable' | 'ExpandableSteps';
  ContentId: number;
  AnchorLink: string;
  ContentBlocks: ContentBlock[];
}
export interface LifeEvent extends ItemBaseProps {
  'Levensgebeurtenis.Id': number;
  'Levensgebeurtenis.Titel': string;
  'Levensgebeurtenis.Omschrijving': string;
  'Levensgebeurtenis.Relevante_Filters': string[];
  'Levensgebeurtenis.Content': Array<Content>;
}

export interface Regulation extends ItemBaseProps {
  'Regeling.Id': number;
  'Regeling.Naam': string;
  MainImage: string;
  'Regeling.Grondslag': string;
  'Regeling.Titel': string;
  'Regeling.Subtitel': string;
  'Regeling.Content': Array<Content>;
  'Regeling.Relevante_Grondslagen': [
    {
      label: string;
      grondslag: string;
      link: string;
    },
  ];
  'Regeling.Functionaliteiten': Array<{
    Functionaliteit: Functionaliteit;
    Titel: string;
    Onderwerp: string;
    Leestijd: number;
    Intro: string;
    Stappen_Koptekst: string;
    Stappen: Array<{
      Volgorde: number;
      Naam: string;
      Titel: string;
      Omschrijving: string;
    }>;
  }>;
  'Regeling.Bijbehorende_Levensgebeurtenis': number;
  'Regeling.Relevante_Levensgebeurtenissen': number[];
  'Regeling.Authoriteitsoort': string;
  Wordt_Onderhouden_Door: string;
  DefaultSpeachBulb: {
    Body: string;
    HasHyperlink: boolean;
    HyperlinkRef: string;
    HyperlinkText: string;
    Title: string;
  };
  'Regeling.Doelgroep': string;
  'Regeling.Dev.Bron': string;
  'Regeling.Dev.Bronrij': number;
  'Regeling.Dev.Waarschijnlijke_UPL': string;
  'Regeling.Dev.Relevante_UPLs': string[];
  'Regeling.Dev.Bestandsnaam': string;
  'Regeling.Dev.Source_URL': string;
}

export type Item = LifeEvent | Regulation;
