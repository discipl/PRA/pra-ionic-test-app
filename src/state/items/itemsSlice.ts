import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState } from '../store';

import fetchItems, { type ItemResponse } from '../../endpoints/fetchItem';
import { type Item } from './itemsInterface';

/**
 * The initial state for the Items slice
 */
const initialState: Record<number, Item> = {};

/**
 * Selector for getting the Items;
 */
export const selectItems = (state: RootState): Record<number, Item> | undefined => state.items;
export const selectItem =
  (id: number) =>
  (state: RootState): Item | undefined =>
    state.items[id] ?? undefined;

/**
 * Thunk function for fetching the item
 */
export const fetch = createAsyncThunk('items/fetch', async (itemid: number): Promise<ItemResponse> => {
  return await fetchItems({
    id: itemid,
  });
});

/**
 * The ItemsSlice in the redux state
 */
export const ItemsSlice = createSlice({
  name: 'items',
  initialState,
  extraReducers: (builder) => {
    /**
     * Add the Items to the state when the fetch request is completed
     */
    builder.addCase(fetch.fulfilled, (state, action) => {
      const itemsResponse = action.payload;
      return processItemsResponse(state, itemsResponse);
    });
  },
  reducers: {},
});

const processItemsResponse = (state: Record<number, Item>, itemsResponse: ItemResponse): Record<number, Item> => {
  const newItems = state;
  newItems[itemsResponse.Id] = itemsResponse;
  return newItems;
};
export default ItemsSlice.reducer;
