export interface LifeEventsListItem {
  name: string;
  id: number;
  description: string;
}
