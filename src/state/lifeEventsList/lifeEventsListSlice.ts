import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState } from '../store';
import fetchContent, { type ContentResponse, type ContentResponseItem } from '../../endpoints/fetchContent';
import { type LifeEventsListItem } from './lifeEventsListInterface';

/**
 * The initial state for the lifeEventsList slice
 */
const initialState: LifeEventsListItem[] = [];

/**
 * Selector for getting the LifeEventsList;
 */
export const selectLifeEventsList = (state: RootState): LifeEventsListItem[] | undefined => state?.lifeEventsList;

/**
 * Thunk function for fetching the lifeEventsList
 */
export const fetchLifeEventsList = createAsyncThunk('lifeEventsList/fetch', async (): Promise<ContentResponse> => {
  return await fetchContent({ type_filter: 'Levensgebeurtenis', n: 1000 });
});

/**
 * The LifeEventsListSlice in the redux state
 */
export const LifeEventsListSlice = createSlice({
  name: 'lifeEventsList',
  initialState,
  extraReducers: (builder) => {
    /**
     * Add the LifeEventsList to the state when the fetch request is completed
     */
    builder.addCase(fetchLifeEventsList.fulfilled, (state, action) => {
      const contentResponse = action.payload;
      return processItemsResponse(state, contentResponse);
    });
  },
  reducers: {},
});

const processItemsResponse = (state: LifeEventsListItem[], contentResponse: ContentResponse): LifeEventsListItem[] => {
  const lifeEventsList: LifeEventsListItem[] = [];
  contentResponse?.results?.forEach((item: ContentResponseItem) => {
    const name = item['Zoekdescriptor.Titel']?.length > 0 ? item['Zoekdescriptor.Titel'] : '';
    const id = item?.Id !== '' ? item.Id : '';
    const description =
      item['Zoekdescriptor.Korte_Omschrijving']?.length > 0 ? item['Zoekdescriptor.Korte_Omschrijving'] : '';

    const lifeEventsListItem: LifeEventsListItem = {
      name,
      id,
      description,
    };
    lifeEventsList.push(lifeEventsListItem);
  });

  return lifeEventsList;
};
export default LifeEventsListSlice.reducer;
