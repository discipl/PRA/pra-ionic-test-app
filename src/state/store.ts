import { combineReducers, configureStore } from '@reduxjs/toolkit';
import appStateReducer from './app/appSlice';
import tasksReducer from './tasks/tasksSlice';
import themesReducer from './themesList/themesListSlice';
import itemsReducer from './items/itemsSlice';
import LifeEventsListReducer from './lifeEventsList/lifeEventsListSlice';
import profileStateReducer from './profile/profileSlice';
import searchResultsReducer from './searchResults/searchResultsSlice';
import searchTermsReducer from './searchTerms/searchTermsSlice';
import SearchFiltersReducer from './searchFilters/searchFiltersSlice';
import savedItemsReducer from './savedItems/savedItemsSlice';
import modelExecutorReducer from './modelExecutor/modelExecutorSlice';

import { persistStore, persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  version: 1,
  storage,
  whitelist: ['searchFilters', 'app', 'profile', 'savedItems', 'modelExecutor'],
};

/**
 * Combine the reducers
 */
const reducer = combineReducers({
  app: appStateReducer,
  profile: profileStateReducer,
  items: itemsReducer,
  lifeEventsList: LifeEventsListReducer,
  searchResults: searchResultsReducer,
  searchFilters: SearchFiltersReducer,
  searchTerms: searchTermsReducer,
  tasks: tasksReducer,
  themesList: themesReducer,
  savedItems: savedItemsReducer,
  modelExecutor: modelExecutorReducer,
});

const persistedReducer = persistReducer(persistConfig, reducer);

/**
 * Configure the redux store
 */
export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;

// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
