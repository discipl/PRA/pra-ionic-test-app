import { createSlice, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';
import { type SavedItem, type SavedItemsSlice } from './savedItemsInterface';

/**
 * The initial state for the Savedtems slice
 */
const initialState: SavedItemsSlice = { lifeEvents: [], news: [], regulations: [] };

/**
 * Selector for getting the Savedtems;
 */
export const selectSavedItems = (state: RootState): SavedItemsSlice => state.savedItems;
export const isItemSaved =
  ({ type, id: itemId }: { type: 'lifeEvent' | 'news' | 'regulation'; id: number }) =>
  (state: RootState): boolean => {
    if (type === 'lifeEvent') {
      return state.savedItems.lifeEvents.findIndex(({ id }) => id === itemId) > -1;
    } else if (type === 'regulation') {
      return state.savedItems.regulations.findIndex(({ id }) => id === itemId) > -1;
    } else if (type === 'news') {
      return state.savedItems.news.findIndex(({ id }) => id === itemId) > -1;
    }
    return false;
  };

/**
 * The SavedtemsSlice in the redux state
 */
export const SavedtemsSlice = createSlice({
  name: 'savedItems',
  initialState,
  reducers: {
    saveOrRemoveItem: (state, action: PayloadAction<SavedItem>) => {
      const savedItem = action.payload;
      const { type: itemType, id: itemId } = savedItem;
      if (itemType === 'lifeEvent' && itemId !== undefined) {
        const foundId = state.lifeEvents.findIndex(({ id }) => id === itemId);
        if (foundId === -1) {
          state.lifeEvents.push(savedItem);
        } else {
          state.lifeEvents.splice(foundId, 1);
        }
      } else if (itemType === 'news' && itemId !== undefined) {
        const foundId = state.news.findIndex(({ id }) => id === itemId);
        if (foundId === -1) {
          state.news.push(savedItem);
        } else {
          state.news.splice(foundId, 1);
        }
      } else if (itemType === 'regulation' && itemId !== undefined) {
        const foundId = state.regulations.findIndex(({ id }) => id === itemId);
        if (foundId === -1) {
          state.regulations.push(savedItem);
        } else {
          state.regulations.splice(foundId, 1);
        }
      }
      return state;
    },
  },
});
export const { saveOrRemoveItem } = SavedtemsSlice.actions;
export default SavedtemsSlice.reducer;
