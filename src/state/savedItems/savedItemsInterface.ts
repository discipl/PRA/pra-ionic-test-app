export interface SavedItemBaseProps {
  id: number;
}
export interface Regulation extends SavedItemBaseProps {
  type: 'regulation';
  title?: string;
  message?: string;
}
export interface LifeEvent extends SavedItemBaseProps {
  type: 'lifeEvent';
  label?: string;
  imageSrc?: string;
}
export interface NewsItem extends SavedItemBaseProps {
  type: 'news';
  title?: string;
  message?: string;
  notificationMessage?: string;
}

export type SavedItem = Regulation | LifeEvent | NewsItem;

export interface SavedItemsSlice {
  regulations: Regulation[];
  news: NewsItem[];
  lifeEvents: LifeEvent[];
}
