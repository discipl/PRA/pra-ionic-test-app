import { type SearchFilter } from './../searchFilters/searchFiltersInterface';
import { createSlice, createAsyncThunk, type PayloadAction } from '@reduxjs/toolkit';
import type { RootState } from '../store';

import fetchContent, {
  type ContentRequestBody,
  type ContentResponse,
  type ContentResponseItem,
} from '../../endpoints/fetchContent';
import { type SearchResult, type SearchSlice } from './searchResultsInterface';
import { upsertSearchFilter } from '../searchFilters/searchFiltersSlice';

/**
 * The initial state for the SearchResults slice
 */
const initialState: SearchSlice = { activeFilterId: '_unsavedFilters' };

/**
 * Selector for getting the SearchResults;
 */
export const selectSearchResults = (state: RootState): SearchSlice => state.searchResults;
/**
 * Selector for getting the active filter id;
 */
export const selectActiveFilterId = (state: RootState): string | undefined => state.searchResults.activeFilterId;

/**
 * Thunk function for fetching the SearchResults
 */
export const search = createAsyncThunk(
  'searchResults/search',
  async ({
    searchquery,
    filters,
    themes,
    start,
    n,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    filter_ids,
    // eslint-disable-next-line @typescript-eslint/naming-convention
    relevant_ids,
  }: ContentRequestBody): Promise<ContentResponse> => {
    return await fetchContent({
      searchquery,
      filters,
      themes,
      start,
      n,
      filter_ids,
      relevant_ids,
    });
  },
);
export const setActiveFilterAndSearch = createAsyncThunk(
  'searchResults/setActiveFilterAndSearch',
  async (
    {
      activeFilterId,
      searchquery,
    }: {
      activeFilterId: string | undefined;
      searchquery: string | undefined;
    },
    { getState, dispatch },
  ): Promise<void> => {
    dispatch(setActiveFilterId(activeFilterId));
    let searchFilter: SearchFilter | undefined;
    if (activeFilterId !== undefined) {
      const state = getState() as RootState;
      searchFilter = state.searchFilters.find((filter) => filter.id === activeFilterId);
    }
    await dispatch(search({ themes: searchFilter?.themes, searchquery }));
  },
);
export const upsertUnsavedFilterAndSearch = createAsyncThunk(
  'searchResults/upsertUnsavedFilterAndSearch',
  async (
    { searchFilter, searchquery }: { searchFilter: SearchFilter; searchquery: string },
    { dispatch },
  ): Promise<void> => {
    searchFilter.id = '_unsavedFilters';
    dispatch(upsertSearchFilter(searchFilter));
    dispatch(setActiveFilterId('_unsavedFilters'));
    await dispatch(search({ themes: searchFilter.themes, searchquery }));
  },
);

/**
 * The SearchResultsSlice in the redux state
 */
export const SearchResultsSlice = createSlice({
  name: 'searchResults',
  initialState,
  extraReducers: (builder) => {
    /**
     * Add the SearchResults to the state when the fetch request is completed
     */
    builder.addCase(search.fulfilled, (state, action) => {
      const ContentResponse = action.payload;
      return processContentResponse(state, ContentResponse);
    });
  },
  reducers: {
    setActiveFilterId: (state, action: PayloadAction<string | undefined>) => {
      const filterId = action.payload;
      if (filterId != null) {
        state.activeFilterId = filterId;
      } else {
        state.activeFilterId = '_unsavedFilters';
      }
    },
  },
});
export const { setActiveFilterId } = SearchResultsSlice.actions;

const processContentResponse = (state: SearchSlice, ContentResponse: ContentResponse): SearchSlice => {
  const { results, totalResults, themes, searchquery } = ContentResponse;
  const searchResults = results.map((searchResult: ContentResponseItem): SearchResult => {
    const {
      'Zoekdescriptor.Titel': title,
      'Zoekdescriptor.Korte_Omschrijving': descriptionShort,
      'Zoekdescriptor.Lange_Omschrijving': descriptionLong,
      Id: id,
      Wordt_Onderhouden_Door: authority,
    } = searchResult;
    return {
      title,
      descriptionShort,
      descriptionLong,
      authority,
      id,
    };
  });

  return {
    ...state,
    totalResults,
    themes,
    searchquery,
    results: searchResults,
  };
};
export default SearchResultsSlice.reducer;
