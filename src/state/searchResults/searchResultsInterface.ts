export interface SearchSlice {
  queryTerms?: string[];
  subjectsFilter?: string[];
  totalResults?: number;
  results?: SearchResult[];
  activeFilterId?: string;
  themes?: string[];
  searchquery?: string;
}
export interface SearchResult {
  title: string;
  descriptionShort: string;
  descriptionLong: string;
  authority: string;
  id: number;
}
