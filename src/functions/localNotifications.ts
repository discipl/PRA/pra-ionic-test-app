import { LocalNotifications, type LocalNotificationSchema } from '@capacitor/local-notifications';

const generateId = (): number => Math.floor(Math.random() * 10);

export const scheduleNotification = async ({
  title,
  body,
  schedule,
  extra,
}: {
  title: string;
  body: string;
  schedule: { at: Date };
  extra: object;
}): Promise<void> => {
  const notifications: LocalNotificationSchema[] = [
    {
      id: generateId(),
      title,
      body,
      sound: 'beep.aiff',
      schedule,
      extra: extra,
    },
  ];

  await LocalNotifications.schedule({ notifications });
};

export const scheduleNotificationIn10Seconds = async ({
  title,
  body,
  extra,
}: {
  title: string;
  body: string;
  extra: object;
}): Promise<void> => {
  let scheduleDate = new Date();
  scheduleDate.setSeconds(scheduleDate.getSeconds() + 1);
  await scheduleNotification({ title, body, schedule: { at: scheduleDate }, extra });
};

export const ensurePermissions = async (): Promise<PermissionState> => {
  try {
    let { display } = await LocalNotifications.checkPermissions();

    if (display === 'prompt') {
      ({ display } = await LocalNotifications.requestPermissions());
    }

    if (display !== 'granted') {
      throw new Error('User denied permissions!');
    }

    return display;
  } catch (e) {
    console.log('permissions error');
    console.error(e);

    return 'denied';
  }
};
