import ChevronRight from '@persoonlijke-regelingen-assistent/assets/dist/icons/functioneel/ChevronRight';
import {
  Button,
  Heading,
  Paragraph,
  UnorderedList,
  UnorderedListItem,
} from '@persoonlijke-regelingen-assistent/components-react';
import parse, {
  type HTMLReactParserOptions,
  type DOMNode,
  type Element,
  type Text,
  domToReact,
} from 'html-react-parser';

const baseURL = import.meta.env?.BASE_URL ?? '/';

const isElement = (domNode: DOMNode | undefined): domNode is Element | undefined => {
  return domNode != null && 'attribs' in domNode;
};
const isText = (domNode: DOMNode | undefined): domNode is Text | undefined => {
  return domNode?.type === 'text';
};

const htmlContentBlockRenderer = (htmlString: string): string | JSX.Element | JSX.Element[] => {
  const options: HTMLReactParserOptions = {
    replace(domNode: DOMNode) {
      if (isElement(domNode)) {
        const { tagName, children } = domNode;
        const childrenToReact = domToReact(children as DOMNode[], options);
        console.log('domNode?.attribs?.href', domNode?.attribs?.href);
        switch (tagName) {
          case 'h1':
            return <Heading level={1}>{childrenToReact}</Heading>;
          case 'h2':
            return <Heading level={2}>{childrenToReact}</Heading>;
          case 'h3':
            return <Heading level={3}>{childrenToReact}</Heading>;
          case 'h4':
            return <Heading level={4}>{childrenToReact}</Heading>;
          case 'h5':
            return <Heading level={5}>{childrenToReact}</Heading>;
          case 'h6':
            return <Heading level={6}>{childrenToReact}</Heading>;
          case 'ul':
            return <UnorderedList>{childrenToReact}</UnorderedList>;
          case 'a':
            return <a href={domNode?.attribs?.href}>{childrenToReact}</a>;
          case 'b':
            return <b>{childrenToReact}</b>;
          case 'strong':
            return <strong>{childrenToReact}</strong>;
          case 'i':
            return <i>{childrenToReact}</i>;
          case 'em':
            return <em>{childrenToReact}</em>;
          case 'br':
            return <br />;
          case 'li':
            return <UnorderedListItem markerContent={ChevronRight({})}>{childrenToReact}</UnorderedListItem>;
          case 'p':
            return <Paragraph>{childrenToReact}</Paragraph>;
          case 'button':
            return (
              <form
                style={{
                  minInlineSize: '100%',
                }}
              >
                <Button
                  appearance="primary"
                  style={{
                    backgroundColor: 'var(--pra-color-primary-600)',
                    color: 'var(--voorbeeld-color-white)',
                    minInlineSize: '100%',
                    cursor: 'pointer',
                    marginBlock: '20px',
                  }}
                  onClick={() => (window.location.href = `${baseURL}${domNode?.attribs?.href}`)}
                >
                  {childrenToReact}
                </Button>
              </form>
            );
          default:
            return <>{childrenToReact}</>;
        }
      } else if (
        isText(domNode) &&
        (domNode.parentNode == null ||
          !['a', 'p', 'li', 'b', 'strong', 'em', 'i', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'button'].includes(
            (domNode?.parentNode as Element).name,
          )) &&
        typeof domNode.data === 'string' &&
        domNode.data.trim().length > 0
      ) {
        return <Paragraph>{domNode.data}</Paragraph>;
      }
    },
  };

  return parse(htmlString, options);
};
export default htmlContentBlockRenderer;
