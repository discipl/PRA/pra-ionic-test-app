import { type SVGProps } from 'react';
import * as iconsImport from '@persoonlijke-regelingen-assistent/assets/dist/icons/toptaak';

const icons = iconsImport as Record<string, (props: SVGProps<SVGSVGElement>) => JSX.Element>;
const nameToIconMapper = (name: string): ((props: SVGProps<SVGSVGElement>) => JSX.Element) | undefined => {
  return icons[name];
};

export default nameToIconMapper;
