import birthday_party from '@persoonlijke-regelingen-assistent/assets/dist/images/birthday-party.png';
import error from '@persoonlijke-regelingen-assistent/assets/dist/images/error.png';
import levensgebeurtenis from '@persoonlijke-regelingen-assistent/assets/dist/images/levensgebeurtenis.png';
import man_with_laptop from '@persoonlijke-regelingen-assistent/assets/dist/images/man-with-laptop.png';
import muslim_wedding from '@persoonlijke-regelingen-assistent/assets/dist/images/muslim-wedding.png';
import pregnancy from '@persoonlijke-regelingen-assistent/assets/dist/images/pregnancy.png';
import retirement_check from '@persoonlijke-regelingen-assistent/assets/dist/images/retirement-check.png';
import student_graduation from '@persoonlijke-regelingen-assistent/assets/dist/images/student-graduation.png';
import user_login from '@persoonlijke-regelingen-assistent/assets/dist/images/user-login.png';
import user_todo from '@persoonlijke-regelingen-assistent/assets/dist/images/user-todo.png';
import user_welcome from '@persoonlijke-regelingen-assistent/assets/dist/images/user-welcome.png';
import web_developer from '@persoonlijke-regelingen-assistent/assets/dist/images/web-developer.png';

const nameToImageMapper: { [key: string]: string } = {
  'birthday-party': birthday_party,
  error: error,
  levensgebeurtenis: levensgebeurtenis,
  'man-with-laptop': man_with_laptop,
  'muslim-wedding': muslim_wedding,
  pregnancy: pregnancy,
  'retirement-check': retirement_check,
  'student-graduation': student_graduation,
  'user-login': user_login,
  'user-todo': user_todo,
  'user-welcome': user_welcome,
  'web-developer': web_developer,
};
export default nameToImageMapper;
