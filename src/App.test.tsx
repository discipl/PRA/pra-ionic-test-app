import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { Provider } from 'react-redux';
import { store } from './state/store';

describe('[App]', () => {
  it('renders without crashing', () => {
    const { baseElement } = render(
      <Provider store={store}>
        <App />
      </Provider>,
    );
    expect(baseElement).toBeDefined();
  });
});
