import '@persoonlijke-regelingen-assistent/design-tokens/dist/default/index.css';
import Router from './router/Router';
import './App.scss';
import '@persoonlijke-regelingen-assistent/font/src/index.scss';

const App: React.FC = () => {
  return (
    <div className="pra-theme pra-app">
      <Router />
    </div>
  );
};

export default App;
