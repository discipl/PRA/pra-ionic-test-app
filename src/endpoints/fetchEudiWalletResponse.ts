import { environment } from '../environment/environment';
import { type PresentationsResponse } from '../typescript-definitions/eudi-wallet';

export const fetchEudiWalletResponse = async (
  presentationId: string,
  code?: string,
): Promise<PresentationsResponse> => {
  const URL =
    code != null
      ? `${environment.eudiApiUrl}/ui/presentations/${presentationId}?response_code=${code}`
      : `${environment.eudiApiUrl}/ui/presentations/${presentationId}`;
  const method = 'GET';
  try {
    const response = await fetch(URL, { method });
    const json = await response.json();

    if (response.ok) {
      return json;
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
};
