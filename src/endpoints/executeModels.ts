import { plainAsync, traceableAsync, start } from 'https://discipl.gitlab.io/PRA/flint-client/lib/v1/model-executor.js';
import { ModelResultValue } from '../state/modelExecutor/modelExecutorInterface';

export async function startModelExecutor() {
  await start(`https://discipl.gitlab.io/PRA/flint-client/models/v1/`);
}
export async function execute(model: string, functionality: string, inputs: object) {
  if (functionality === 'notificatie') {
    const variables = await traceableAsync(model, functionality, inputs);
    return variables[0].DagenTotPensioen.leaf[0].dagen[0];
    //TODO
  } else {
    const variables = await plainAsync(model, functionality, inputs);
    const fullNameVariables = Object.keys(variables[0]).reduce(
      (fullNameVariables: Record<string, ModelResultValue>, variableName) => {
        if (variableName !== '$tag') {
          const fullName = `${model}/${functionality}/${variableName}`;
          const value = variables[0][variableName];
          if (Array.isArray(value)) {
            fullNameVariables[fullName] = value.map((_valueItem: any) => {
              return getPlainValue(_valueItem[_valueItem.functor]);
            });
          } else if (typeof value === 'object' && value !== null) {
            fullNameVariables[fullName] = getPlainValue(value[variables[0][variableName].functor]);
          } else {
            fullNameVariables[fullName] = getPlainValue(value);
          }
        }
        return fullNameVariables;
      },
      {},
    );
    return fullNameVariables;
  }
}

const getPlainValue = (value: any[]) => {
  if (Array.isArray(value)) {
    if (value.length === 1) {
      return value[0];
    } else {
      return value.join('-');
    }
  }
  return value;
};
