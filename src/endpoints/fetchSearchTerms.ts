export interface SearchTermsResponse {
  themes?: string[];
  subjects?: string[];
  ministeries?: string[];
}

export interface SearchTermsRequestBody {
  themes?: string[];
  subjects?: string[];
  ministeries?: string[];
}

export default async function fetchSearchTerms(requestBody: SearchTermsRequestBody): Promise<SearchTermsResponse> {
  const URL = `${import.meta.env.VITE_BACK_END_URL}/browse/nl-NL`;
  const method = 'POST';
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  const body = {
    ...requestBody,
  };

  try {
    const response = await fetch(URL, {
      method,
      body: JSON.stringify(body),
      headers,
    });
    const json = await response.json();

    if (response.ok) {
      return json;
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
}
