export interface ContentResponseItem {
  Id: string;
  Type: string;
  Wordt_Onderhouden_Door: string;
  'Zoekdescriptor.Laatst_Bewerkt_Op': string;
  'Zoekdescriptor.Taal': string;
  'Zoekdescriptor.Variant': string;
  'Zoekdescriptor.Model': string;
  'Zoekdescriptor.Titel': string;
  'Zoekdescriptor.Lange_Omschrijving': string;
  'Zoekdescriptor.Korte_Omschrijving': string;
  'Zoekdescriptor.Filters': string;
  'Zoekdescriptor.Zoektermen': string;
}

export interface ContentResponse {
  searchquery: string;
  from: number;
  to: number;
  totalResults: number;
  themes?: string[];
  results: ContentResponseItem[];
}

export interface ContentRequestBody {
  searchquery?: string;
  start?: number;
  themes?: string[];
  n?: number;
  filters?: string[];
  filter_ids?: string[];
  relevant_ids?: string[];
  type_filter?: string;
}

export default async function fetchContent(contentRequestBody: ContentRequestBody): Promise<ContentResponse> {
  const URL = `${import.meta.env.VITE_BACK_END_URL}/content`;
  const method = 'POST';
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  const body = {
    ...contentRequestBody,
  };

  try {
    const response = await fetch(URL, {
      method,
      body: JSON.stringify(body),
      headers,
    });
    const json: ContentResponse = await response.json();

    if (response.ok) {
      return json;
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
}
