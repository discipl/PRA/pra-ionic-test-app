export interface CheckNotificationsResponse {
  'Item.Id'?: number;
  'Item.Taal'?: string[];
  'Notificatie.Id'?: number;
  'Notificatie.Tekst'?: string;
}

export interface CheckNotificationsRequestBodyContext {
  String?: Record<string, string>;
  StringCollection?: Record<string, string[]>;
  Boolean?: Record<string, boolean>;
}

export default async function checkNotificaitons(
  requestBodyContext: CheckNotificationsRequestBodyContext,
): Promise<CheckNotificationsResponse> {
  const URL = `${import.meta.env.VITE_BACK_END_URL}/check-notifications`;
  const method = 'POST';
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  const body = {
    context: { ...requestBodyContext },
  };

  try {
    const response = await fetch(URL, {
      method,
      body: JSON.stringify(body),
      headers,
    });
    const json = await response.json();

    if (response.ok) {
      return json?.results?.[0];
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
}
