import { type Item } from '../state/items/itemsInterface';

export type ItemResponse = Item;

export interface ItemRequestBody {
  id: number;
}

export default async function fetchItems(requestBody: ItemRequestBody): Promise<ItemResponse> {
  const URL = `${import.meta.env.VITE_BACK_END_URL}/item`;
  const method = 'POST';
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  const body = {
    ...requestBody,
  };

  try {
    const response = await fetch(URL, {
      method,
      body: JSON.stringify(body),
      headers,
    });
    const json = await response.json();

    if (response.ok) {
      return json;
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
}
