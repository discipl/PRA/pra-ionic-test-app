import { environment } from '../environment/environment';
import { type ScenarioDefinition, type PresentationDefinitionResponse } from '../typescript-definitions/eudi-wallet';

export const initializeEudiWalletTransaction = async (
  payload: ScenarioDefinition,
): Promise<PresentationDefinitionResponse> => {
  try {
    const URL = `${environment.eudiApiUrl}/ui/presentations`;
    const method = 'POST';
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };
    const body = {
      ...payload,
    };
    const response = await fetch(URL, {
      method,
      body: JSON.stringify(body),
      headers,
    });
    const json = await response.json();

    if (response.ok) {
      return json;
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
};
