export interface CategoriesResponseItem {
  Compleet: boolean;
  Id: number;
  Wordt_Onderhouden_Door: string;
  Type: string;
  'Categorie.Laatst_Bewerkt_Op': string;
  'Categorie.Soort': string;
  'Categorie.Id': 0;
  'Categorie.Icon': string;
  'Categorie.Taal': string;
  'Categorie.Handle': string;
  'Categorie.Titel': string;
  'Categorie.Filters': Array<{
    Type: string;
    Id: number;
    Handle: string;
    Titel: string;
  }>;
}
export interface CategoriesResponse {
  results: CategoriesResponseItem[];
}

export interface CategoriesRequestBody {
  kind?: string;
  language?: string;
}

export default async function fetchCategories(requestBody: CategoriesRequestBody): Promise<CategoriesResponse> {
  const URL = `${import.meta.env.VITE_BACK_END_URL}/categories`;
  const method = 'POST';
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  const body = {
    ...requestBody,
  };

  try {
    const response = await fetch(URL, {
      method,
      body: JSON.stringify(body),
      headers,
    });
    const json = await response.json();

    if (response.ok) {
      return json;
    }
    throw new Error('Request has no response.');
  } catch (error) {
    console.error(error);
    throw error;
  }
}
