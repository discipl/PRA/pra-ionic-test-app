import { PraNavigationBar } from '@persoonlijke-regelingen-assistent/components-react';
import { useHistory } from 'react-router';

export interface NavigationProps {
  selectedItem?: '' | 'Dashboard' | 'Profiel' | 'Instellingen';
  indicators?: Array<'Dashboard' | 'Profiel' | 'Instellingen'>;
}
function MenuNavigation(props: NavigationProps): ReturnType<React.FC> {
  const { selectedItem, indicators } = props;
  const history = useHistory();

  return (
    <PraNavigationBar
      dashboardOnClickHandler={() => {
        history.push('/dashboard');
      }}
      settingsOnClickHandler={() => {
        history.push('/settings');
      }}
      profileOnClickHandler={() => {
        history.push('/saved-items');
      }}
      selectedItem={selectedItem}
      indicators={indicators}
    />
  );
}

export default MenuNavigation;
