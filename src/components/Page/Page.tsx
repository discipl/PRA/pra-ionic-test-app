import { type ForwardedRef, forwardRef, type HTMLAttributes, type PropsWithChildren, type ReactNode } from 'react';
import './Page.scss';

import MenuNavigation from '../MenuNavigation/MenuNavigation';
import { Header } from '@persoonlijke-regelingen-assistent/components-react';
interface PageSpecificProps {
  alternateTheme?: boolean;
  backgroundFC?: React.FC;
  navigationHide?: boolean;
  navigationSelectedItem?: string;
}
export interface PageProps extends HTMLAttributes<HTMLDivElement>, PageSpecificProps {}

export const Page = forwardRef(
  (
    {
      children,
      navigationSelectedItem,
      navigationHide,
      backgroundFC,
      alternateTheme,
      ...props
    }: PropsWithChildren<PageProps>,
    ref: ForwardedRef<HTMLDivElement>,
  ) => {
    return (
      <div className={`${alternateTheme ?? false ? 'pra-alternate-theme' : ''} pra-page`} ref={ref} {...props}>
        {backgroundFC?.({ className: 'pra-page-background' })}
        <header>
          <Header
          />
        </header>
        <main>{children}</main>
        <footer>
          {!(navigationHide ?? false) && <MenuNavigation navigationSelectedItem={navigationSelectedItem ?? ''} />}
        </footer>
      </div>
    );
  },
);

Page.displayName = 'Page';
