import React from 'react';
import './Spinner.scss';
import spinnerGif from '@assets/images/spinner.gif';

function Spinner(): ReturnType<React.FC> {
  return (
    <div className="spinner-container">
      <img className="spinner" src={spinnerGif} alt="spinner" />
    </div>
  );
}

export default Spinner;
