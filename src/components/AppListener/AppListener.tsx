import { App, URLOpenListenerEvent } from '@capacitor/app';
import { LocalNotifications } from '@capacitor/local-notifications';
import { useEffect } from 'react';
import { useHistory } from 'react-router';

const AppListener: React.FC<any> = () => {
  let history = useHistory();
  useEffect(() => {
    App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
      // Example url: 192.168.68.110:5173/tabs/tab2
      // slug = /tabs/tab2
      const slug = event.url.split('192.168.68.110:5173').pop();
      if (slug) {
        history.push(slug);
      }
    });
    LocalNotifications.addListener('localNotificationActionPerformed', async (action) => {
      const regulationId = action?.notification?.extra?.regulationId;
      history.push('/dashboard');
      if (regulationId != null) {
        history.push(`/regulations/${regulationId}`);
      }
    });
  }, []);

  return null;
};

export default AppListener;
