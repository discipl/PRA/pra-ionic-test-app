export interface PresentationDefinitionResponse {
  client_id: string;
  request_uri: string;
  presentation_id: string;
}

export interface PresentationsResponse {
  presentation_submission: any;
  vp_token?: string;
  id_token?: string;
}
export interface WalletResponse {
  vp_token?: string;
  id_token?: string;
}

export interface TransformedResponse {
  idToken: Array<Record<string, string>>;
  vpToken: Array<Record<string, string>>;
}

export interface DefinitionPath {
  path: string[];
  intent_to_retain: boolean;
}

export interface ScenarioDefinition {
  type: string;
  presentation_definition: {
    id: string;
    input_descriptors: Array<{
      id: string;
      name: string;
      purpose: string;
      format: {
        mso_mdoc: {
          alg: string[];
        };
      };
      constraints: {
        fields: DefinitionPath[];
      };
    }>;
  };
  nonce: string;
  wallet_response_redirect_uri_template?: string;
}
