import { test, expect } from "@playwright/test";

test.describe("Dashboard test", () => {
  test.beforeEach(async ({ page, baseURL }) => {
    await page.goto(baseURL ?? "");
    await page.waitForSelector(".pra-app");
  });

  test("has title", async ({ page }) => {
    // Expect a title "to contain" a substring.
    await expect(page).toHaveTitle(/PRA/);
  });

  test("shows the dashboard page", async ({ page }) => {
    const headerTitle = page.locator(".pra-header-bar-title");
    await expect(headerTitle).toHaveText("Dashboard");
  });

  test("opens profile page", async ({ page }) => {
    const profileButton = page.locator(
      "li:nth-child(4) > .pra-navigation-item-icon"
    );
    await profileButton.click();
    const headerTitle = page.locator(".pra-header-bar-title");
    await expect(headerTitle).toHaveText("Profiel");
  });

  test("opens messages page", async ({ page }) => {
    const messagesButton = page.locator(
      "li:nth-child(3) > .pra-navigation-item-icon"
    );
    await messagesButton.click();
    const headerTitle = page.locator(".pra-header-bar-title");
    await expect(headerTitle).toHaveText("Berichten");
  });

  test("opens notifications page", async ({ page }) => {
    const notificationsButton = page.locator(
      "li:nth-child(2) > .pra-navigation-item-icon"
    );
    await notificationsButton.click();
    const headerTitle = page.locator(".pra-header-bar-title");
    await expect(headerTitle).toHaveText("Taken");
  });
});
