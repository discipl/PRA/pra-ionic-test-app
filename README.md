# PRA Front End Appliation

This repository showcases the PRA App. The PRA App is developed using the platform Ionic React. This is the first version of the PRA App.

## Table of contents
1. Persoonlijke Regelingen Assistent (PRA)
1. PRA Design System
1. Techniques
1. Build
1. Run for development
1. Running tests


## Persoonlijke Regelingen Assistent (PRA)
The Persoonlijke Regelingen Assistent (Personal Arrangements Assistant, PRA) is an ICTU project. The project aims to give citizens more insight into government regulations. So that, for example, they can receive the support they are entitled to. Or not being surprised by a tax bill that is higher than they expect. PRA falls under the Innovation in Services program of the Ministry of the Interior.

For more inormation about the PRA, please check: [PRA overview](https://gitlab.com/discipl/PRA/)

## PRA Design System
The PRA application is built using the PRA Design System. A design system based on the [Netherlands Design System](https://nldesignsystem.nl/). For more information, please check the following sources:

- [PRA Design System Figma](https://www.figma.com/design/K28UEBlBvnfClzUm9d4DYU/PRA---Einddocument)
- [PRA Design System Gitlab](https://gitlab.com/discipl/PRA/pra-design-system)
- [NL Design System Github](https://github.com/nl-design-system)


## Techniques

- Ionic React
- Capacitor
- Redux Toolkit
- Vite
- Jest
- [Eudi Reference Wallet](https://github.com/eu-digital-identity-wallet)

### Ionic plugins

- Local Notifications: is used for sending Task related Notificaitons
- Deeplinking: is used for re-opening the app after receiving the wallet data.

## Prerequisite
- Node.js (>=v20) and npm
- pnpm

## Build
Then you have to install the required packages for this project. 
To do this, run the following command in the project folder (for npm):

```
npm install
```

To Build the project run:

```
npm run build
```

## Run for development
Then you have to install the required packages for this project. 
To do this, run the following command in the project folder (for npm):

```
npm install
```

### Back-end connections
Ensure that all links with the back-end are correct. 
- `executeModels.ts` is connected to the flint-client. Make sure that it's pointed to a working flint-client instead of "https://discipl.gitlab.io/PRA/flint-client"
- Create a file: `.env.development.local` that contains two environment variables, and make sure that these values are correct:
```
VITE_BACK_END_URL=https://pra-backend.germanywestcentral.cloudapp.azure.com
```

### Web

To run this project as a web application use the following command:

```
ionic serve
```

### Android
To run this project as an Android webview app on a mobile device or an emulator, make sure to that you can run an Capacitor Android app.
For more informatiion please check the [documentation](https://capacitorjs.com/docs/android).
When everything is set up correctly and your run the following command to build the app on your device / emulator:

```
npm run build
ionic capacitor add android
ionic capacitor run android --external --livereload -p 5173
```
#### Deeplinking for development on Android

To make sure that deeplinking (opening the app using a link, for example from the wallet app) you should apply this workaround for development:
1. Make sure that the following files point to the correct url and port:
    1. AndroidManifest.xml line 13 (android/app/src/main) android:host="<host IP address of the application>" android:port="<port of the application>"
    1. AppListener.tsx line 12 (src/components/AppListener) make usre the slug points to the right address"
1. Verify the link to the application
    1. When the app is build on your mobile device,.
    1. Open the `App Info` of the app and navigate to `Open by default -> Links to open in this app`, and confirm that your app has your host as a verified link.


### iOS

> NOTE the application is not tested on the iOS platform yet

To run this project as an iOS webview app use the following command:

```
ionic capacitor add ios
ionic cap copy && ionic cap sync && npx cap run ios
```

## Running tests

To run the few this in this project: 

```
npm run test
```

## Demo web app

The demo environment is automatically build with Gitlab Pages.
It can be found here: [https://discipl.gitlab.io/PRA/pra-front-end/](https://discipl.gitlab.io/PRA/pra-front-end/)

## Demo Video

![](./demo-video/belastingdienstVA.mov)
